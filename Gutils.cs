﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics;
using MathNet.Numerics.RootFinding;

namespace TowerDef
{
    public static class Gutils
    {

        public static Random rnd = new Random();

        public static double PointsToAngle(int x1, int y1, int x2, int y2)
        {
            double dx = x1 - x2;
            double dy = y1 - y2;

            return ToAng(dx, dy);
        }

        public static double PointsToAngle(Point p1, Point p2)
        {
            double dx = p1.X - p2.X;
            double dy = p1.Y - p2.Y;

            return ToAng(dx, dy);
        }

        public static double ToAng(double dx, double dy)
        {
            double angl = Math.Atan(dy / dx) * 180 / Math.PI;

            if (dy < 0 && dx > 0 || dy > 0 && dx > 0)
                angl += 180;

            angl = angl * Math.PI / 180;

            return angl;
        }

        public static double FindDist(Point p1, Point p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        public static double FindAngleTarget(double Vp, double Vx, double Vy, int dx, int dy)
        {
            double a = Vx * Vx + Vy * Vy - Vp * Vp;
            double b = 2 * dx * Vx + 2 * dy * Vy;
            double c = dx * dx + dy * dy;
            double d = b * b - 4 * a * c;

            double t = (-b + Math.Sqrt(d)) / (2 * a);

            if (t < 0)
                t = (-b - Math.Sqrt(d)) / (2 * a);

            int pX = Convert.ToInt32(t * Vx + dx) * -1;
            int pY = Convert.ToInt32(t * Vy + dy) * -1;

            return ToAng(pX, pY);
        }

        public static bool CheckCollision(Zombie elem, Bullet aB)
        {
            if (elem.GetLoc().X < aB.GetLoc().X + aB.GetSize().Width && elem.GetLoc().X + elem.GetSize().Width > aB.GetLoc().X && elem.GetLoc().Y < aB.GetLoc().Y + aB.GetSize().Height && elem.GetLoc().Y + elem.GetSize().Height > aB.GetLoc().Y)
                return true;
            else
                return false;
        }

        public static Zombie FindRandomZombie(GameField aGameField)
        {
            Random rnd = new Random();

            return aGameField.Zombies[rnd.Next(0, aGameField.Zombies.Count)];
        }

        public static double FindAngleAcceleration(double Vo, double Vx, double Vy, int dx, int dy, double acc)
        {
            double a = -1 * (acc * acc * acc * acc) / 4;
            double c = Vx * Vx + Vy * Vy - Vo * Vo;
            double d = 2 * dx * Vx + 2 * dy * Vy;
            double e = dx * dx + dy * dy;

            c = c / a;
            d = d / a;
            e = e / a;

            var roots = Cubic.Roots((-1 * (d * d) / 64), ((c * c - 4 * e) / 16), (c / 2), 1);

            Complex root1 = roots.Item1.SquareRoot();
            Complex root2 = roots.Item2.SquareRoot();
            Complex root3 = roots.Item3.SquareRoot();

            List<Complex> rlist = new List<Complex>();
            List<double> reallist = new List<double>();

            rlist.Add(root1 + root2 + root3);
            rlist.Add(root1 + root2 - root3);
            rlist.Add(root1 - root2 + root3);
            rlist.Add(root2 + root3 - root1);
            rlist.Add(root2 - root3 - root1);
            rlist.Add(0 - root1 - root2 - root3);

            for (int i = 0; i < 6; i++)
            {
                if (rlist[i].Imaginary == 0)
                {
                    reallist.Add(rlist[i].Real);
                }
            }

            for (int l = reallist.Count - 1; l > -1; l--)
            {
                if (QuarticEquationCheck((c), (d), (e), reallist[l]) || reallist[l] < 0)
                {
                    reallist.RemoveAt(l);
                }
            }

            //string s = "";
            //foreach (var elem in reallist)
            //    s += elem + ", ";
            //Debug.WriteLine(s);

            double t = reallist.Min();

            int pX = Convert.ToInt32(t * Convert.ToDouble(Vx) + dx) * -1;
            int pY = Convert.ToInt32(t * Convert.ToDouble(Vy) + dy) * -1;
            return ToAng(pX, pY);
        }

        private static bool QuarticEquationCheck(double a, double b, double c, double x)
        {
            double r = Math.Pow(x, 4) + a * Math.Pow(x, 2) + b * x + c;

            if (Math.Abs(r) < Math.Pow(10, -4))
                return false;

            return true;
        }
    }
}