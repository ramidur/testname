﻿
namespace A_Star
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbAlg = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbLines = new System.Windows.Forms.CheckBox();
            this.cbPoints = new System.Windows.Forms.CheckBox();
            this.cbWayPrice = new System.Windows.Forms.CheckBox();
            this.cbPrice = new System.Windows.Forms.CheckBox();
            this.tb_finishx = new System.Windows.Forms.TextBox();
            this.tb_finishy = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_startx = new System.Windows.Forms.TextBox();
            this.tb_starty = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rtNewColors = new System.Windows.Forms.RichTextBox();
            this.lb_wid = new System.Windows.Forms.Label();
            this.btSaveSetup = new System.Windows.Forms.Button();
            this.tbCellWidth = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(911, 550);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tbAlg);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.cbLines);
            this.panel2.Controls.Add(this.cbPoints);
            this.panel2.Controls.Add(this.cbWayPrice);
            this.panel2.Controls.Add(this.cbPrice);
            this.panel2.Controls.Add(this.tb_finishx);
            this.panel2.Controls.Add(this.tb_finishy);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.tb_startx);
            this.panel2.Controls.Add(this.tb_starty);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.rtNewColors);
            this.panel2.Controls.Add(this.lb_wid);
            this.panel2.Controls.Add(this.btSaveSetup);
            this.panel2.Controls.Add(this.tbCellWidth);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(748, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(163, 550);
            this.panel2.TabIndex = 1;
            // 
            // tbAlg
            // 
            this.tbAlg.Location = new System.Drawing.Point(69, 346);
            this.tbAlg.Margin = new System.Windows.Forms.Padding(2);
            this.tbAlg.Name = "tbAlg";
            this.tbAlg.Size = new System.Drawing.Size(52, 20);
            this.tbAlg.TabIndex = 18;
            this.tbAlg.Text = "6";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 353);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "algorithm";
            // 
            // cbLines
            // 
            this.cbLines.AutoSize = true;
            this.cbLines.Location = new System.Drawing.Point(18, 226);
            this.cbLines.Margin = new System.Windows.Forms.Padding(2);
            this.cbLines.Name = "cbLines";
            this.cbLines.Size = new System.Drawing.Size(81, 17);
            this.cbLines.TabIndex = 16;
            this.cbLines.Text = "Show Lines";
            this.cbLines.UseVisualStyleBackColor = true;
            // 
            // cbPoints
            // 
            this.cbPoints.AutoSize = true;
            this.cbPoints.Location = new System.Drawing.Point(18, 205);
            this.cbPoints.Margin = new System.Windows.Forms.Padding(2);
            this.cbPoints.Name = "cbPoints";
            this.cbPoints.Size = new System.Drawing.Size(84, 17);
            this.cbPoints.TabIndex = 15;
            this.cbPoints.Text = "Show points";
            this.cbPoints.UseVisualStyleBackColor = true;
            // 
            // cbWayPrice
            // 
            this.cbWayPrice.AutoSize = true;
            this.cbWayPrice.Location = new System.Drawing.Point(18, 184);
            this.cbWayPrice.Margin = new System.Windows.Forms.Padding(2);
            this.cbWayPrice.Name = "cbWayPrice";
            this.cbWayPrice.Size = new System.Drawing.Size(101, 17);
            this.cbWayPrice.TabIndex = 14;
            this.cbWayPrice.Text = "Show way price";
            this.cbWayPrice.UseVisualStyleBackColor = true;
            // 
            // cbPrice
            // 
            this.cbPrice.AutoSize = true;
            this.cbPrice.Location = new System.Drawing.Point(18, 163);
            this.cbPrice.Margin = new System.Windows.Forms.Padding(2);
            this.cbPrice.Name = "cbPrice";
            this.cbPrice.Size = new System.Drawing.Size(98, 17);
            this.cbPrice.TabIndex = 13;
            this.cbPrice.Text = "Show cell price";
            this.cbPrice.UseVisualStyleBackColor = true;
            // 
            // tb_finishx
            // 
            this.tb_finishx.Location = new System.Drawing.Point(87, 110);
            this.tb_finishx.Margin = new System.Windows.Forms.Padding(2);
            this.tb_finishx.Name = "tb_finishx";
            this.tb_finishx.Size = new System.Drawing.Size(52, 20);
            this.tb_finishx.TabIndex = 12;
            this.tb_finishx.Text = "12";
            // 
            // tb_finishy
            // 
            this.tb_finishy.Location = new System.Drawing.Point(87, 135);
            this.tb_finishy.Margin = new System.Windows.Forms.Padding(2);
            this.tb_finishy.Name = "tb_finishy";
            this.tb_finishy.Size = new System.Drawing.Size(52, 20);
            this.tb_finishy.TabIndex = 11;
            this.tb_finishy.Text = "6";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 135);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "FinishY";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 79);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "StartY";
            // 
            // tb_startx
            // 
            this.tb_startx.Location = new System.Drawing.Point(87, 54);
            this.tb_startx.Margin = new System.Windows.Forms.Padding(2);
            this.tb_startx.Name = "tb_startx";
            this.tb_startx.Size = new System.Drawing.Size(52, 20);
            this.tb_startx.TabIndex = 8;
            this.tb_startx.Text = "9";
            // 
            // tb_starty
            // 
            this.tb_starty.Location = new System.Drawing.Point(87, 79);
            this.tb_starty.Margin = new System.Windows.Forms.Padding(2);
            this.tb_starty.Name = "tb_starty";
            this.tb_starty.Size = new System.Drawing.Size(52, 20);
            this.tb_starty.TabIndex = 7;
            this.tb_starty.Text = "2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 110);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "FinishX";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "StartX";
            // 
            // rtNewColors
            // 
            this.rtNewColors.Location = new System.Drawing.Point(15, 306);
            this.rtNewColors.Margin = new System.Windows.Forms.Padding(2);
            this.rtNewColors.Name = "rtNewColors";
            this.rtNewColors.Size = new System.Drawing.Size(104, 33);
            this.rtNewColors.TabIndex = 4;
            this.rtNewColors.Text = "";
            // 
            // lb_wid
            // 
            this.lb_wid.AutoSize = true;
            this.lb_wid.Location = new System.Drawing.Point(18, 291);
            this.lb_wid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_wid.Name = "lb_wid";
            this.lb_wid.Size = new System.Drawing.Size(55, 13);
            this.lb_wid.TabIndex = 3;
            this.lb_wid.Text = "new width";
            // 
            // btSaveSetup
            // 
            this.btSaveSetup.Location = new System.Drawing.Point(18, 248);
            this.btSaveSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btSaveSetup.Name = "btSaveSetup";
            this.btSaveSetup.Size = new System.Drawing.Size(95, 36);
            this.btSaveSetup.TabIndex = 2;
            this.btSaveSetup.Text = "Update settings";
            this.btSaveSetup.UseVisualStyleBackColor = true;
            this.btSaveSetup.Click += new System.EventHandler(this.btSaveSetup_Click);
            // 
            // tbCellWidth
            // 
            this.tbCellWidth.Location = new System.Drawing.Point(87, 23);
            this.tbCellWidth.Margin = new System.Windows.Forms.Padding(2);
            this.tbCellWidth.Name = "tbCellWidth";
            this.tbCellWidth.Size = new System.Drawing.Size(52, 20);
            this.tbCellWidth.TabIndex = 1;
            this.tbCellWidth.Text = "60";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cell Width";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 550);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbCellWidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btSaveSetup;
        private System.Windows.Forms.Label lb_wid;
        private System.Windows.Forms.RichTextBox rtNewColors;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_startx;
        private System.Windows.Forms.TextBox tb_starty;
        private System.Windows.Forms.TextBox tb_finishx;
        private System.Windows.Forms.TextBox tb_finishy;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbPrice;
        private System.Windows.Forms.CheckBox cbWayPrice;
        private System.Windows.Forms.CheckBox cbPoints;
        private System.Windows.Forms.CheckBox cbLines;
        private System.Windows.Forms.TextBox tbAlg;
        private System.Windows.Forms.Label label6;
    }
}

