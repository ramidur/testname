﻿using AlexUtils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace A_Star
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            LoadVariables();
            Bitmap tempBitmap = new Bitmap(@"Resources\Map.bmp");
            mBitmap = new Bitmap(tempBitmap.Width, tempBitmap.Height);
            using (Graphics g = Graphics.FromImage(mBitmap))
            {
                g.DrawImage(tempBitmap, 0, 0);
            }
        }

        Bitmap mBitmap;
        List<(int color, int value, string text)> mIntValues = new List<(int color, int value, string text)>
        {   (0xFFFFFF, 200, "Ground"),
            (0x000000, 100, "Road"),
            (0x0000FF, 800, "Water"),
            (0x008080, 400, "Forest"),
            (0x808080, 1200, "Mountain"),
        };

        List<List<int>> mMap = new List<List<int>>();
        int mStartx, mStarty, mEndx, mEndy, mCellW, mMapW, mMapH;
        const string C_ConfigFileName = "setting.cfg";

        void LoadVariables()
        {
            var l = AlUtils.LoadFileToList(C_ConfigFileName);
            int idx = 0;

            if (idx < l.Count)
                tbCellWidth.Text = l[idx++];
            if (idx < l.Count)
                tb_startx.Text = l[idx++];
            if (idx < l.Count)
                tb_starty.Text = l[idx++];
            if (idx < l.Count)
                tb_finishx.Text = l[idx++];
            if (idx < l.Count)
                tb_finishy.Text = l[idx++];
            if (idx < l.Count)
                cbPrice.Checked = (l[idx++] == "True");
            if (idx < l.Count)
                cbWayPrice.Checked = (l[idx++] == "True");
            if (idx < l.Count)
                cbPoints.Checked = (l[idx++] == "True");
            if (idx < l.Count)
                cbLines.Checked = (l[idx++] == "True");
            if (idx < l.Count)
                tbAlg.Text = l[idx++];
        }

        void SaveVariables()
        {
            List<string> aList = new List<string>() { tbCellWidth.Text, tb_startx.Text, tb_starty.Text, tb_finishx.Text, tb_finishy.Text, cbPrice.Checked.ToString(), cbWayPrice.Checked.ToString(), cbPoints.Checked.ToString(), cbLines.Checked.ToString(), tbAlg.Text };
            AlUtils.SaveListToFile(C_ConfigFileName, aList);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            RedrawBmp();
        }

        void RedrawBmp()
        {
            panel1.CreateGraphics().DrawImage(mBitmap, 0, 0);
        }

        void UpdateAndShowMap()
        {
            int w = mCellW = StrTo.Int(tbCellWidth.Text);
            Bitmap tempBitmap = new Bitmap(@"Resources\Map.bmp");
            int wp = mMapW = tempBitmap.Width / w;
            int hp = mMapH = tempBitmap.Height / w;
            lb_wid.Text = "new width  " + wp + " / " + hp;

            CreateMap(mMap, wp, hp);

            mBitmap = new Bitmap(wp * w, hp * w);
            using (Graphics g2 = Graphics.FromImage(mBitmap))
            {
                g2.DrawImage(tempBitmap, 0, 0);
            }

            Graphics g = Graphics.FromImage(mBitmap);
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            Brush brush = new SolidBrush(Color.Black);

            List<Color> newColors = new List<Color>();

            for (int y = 0; y < mBitmap.Height - w / 2; y += w)
            {
                for (int x = 0; x < mBitmap.Width - w / 2; x += w)
                {
                    Color cur = tempBitmap.GetPixel(x + w / 2, y + w / 2);
                    int icolor = (cur.R * 256 + cur.G) * 256 + cur.B;

                    int idx = mIntValues.FindIndex(zz => zz.color == icolor);
                    //if (idx<0)
                    //{
                    //    if (!newColors.Contains(cur))
                    //    {
                    //        newColors.Add(cur);
                    //        rtNewColors.Text += "\r" + cur.Name;
                    //    }
                    //    cur = Color.Magenta;
                    //}

                    int value = idx < 0 ? -1 : mIntValues[idx].value;
                    SetToMap(x / w, y / w, value);

                    brush = new SolidBrush(cur);

                    g.FillRectangle(brush, x, y, w, w);
                    if (cbPrice.Checked)
                        g.DrawString("" + value / 100, Font, Brushes.Red, x + 2, y + 2);
                }
            }

            brush = new SolidBrush(Color.Magenta);

            mStartx = StrTo.Int(tb_startx.Text);
            mStarty = StrTo.Int(tb_starty.Text);

            mEndx = StrTo.Int(tb_finishx.Text);
            mEndy = StrTo.Int(tb_finishy.Text);

            g.FillRectangle(brush, mStartx * w, mStarty * w, w, w);
            g.FillRectangle(brush, mEndx * w, mEndy * w, w, w);
            panel1.Refresh();
        }

        private void SetToMap(int x, int y, int v)
        {
            mMap[x + 1][y + 1] = v;
        }

        private void CreateMap(List<List<int>> aMap, int wp, int hp)
        {
            wp += 2;
            hp += 2;
            aMap.Clear();

            for (int x = 0; x < wp; x++)
            {
                List<int> yl = new List<int>();
                aMap.Add(yl);
            }

            for (int y = 0; y < hp; y++)
            {
                for (int xx = 0; xx < wp; xx++)
                {
                    if (y == 0 || y == hp - 1 || xx == 0 || xx == wp - 1)
                        aMap[xx].Add(-1);
                    else
                        aMap[xx].Add(0);
                }
            }
        }
        private void CreateMap0_1(List<List<int>> aMap) //TODO array instead of list
        {
            int hp = mMapH + 2;
            int wp = mMapW + 2;

            for (int y = 0; y < hp; y++)
            {
                List<int> yl = new List<int>();
                aMap.Add(yl);

                for (int xx = 0; xx < wp; xx++)
                {
                    if (mMap[xx][y] == -1 || mMap[xx][y] == 400)
                        aMap[y].Add(0);
                    else
                        aMap[y].Add(1);
                }
            }
        }

        private void btSaveSetup_Click(object sender, EventArgs e)
        {
            SaveVariables();
            UpdateAndShowMap();

            List<(int x, int y)> way = new List<(int x, int y)>();
            switch (tbAlg.Text)
            {
                case "0":
                    way = ShortWayDim(mStartx, mStarty, mEndx, mEndy);
                    break;
                case "1":
                    way = DirectWay(mStartx, mStarty, mEndx, mEndy);
                    break;
                case "2":
                    way = DirectWay2(mStartx, mStarty, mEndx, mEndy);
                    break;
                case "3":
                    way = DirectWay3(mStartx, mStarty, mEndx, mEndy);
                    break;
                case "4":
                    way = BrLine(mStartx, mStarty, mEndx, mEndy);
                    break;
                case "5":
                    way = aStar(mStartx, mStarty, mEndx, mEndy);
                    break;
                case "6":
                    way = ShortWaySan(mStartx, mStarty, mEndx, mEndy);
                    break;
                case "7":
                    way = ShortWay0_1(mStartx, mStarty, mEndx, mEndy);
                    break;
                case "8":
                    way = WikiAstar(mStartx, mStarty, mEndx, mEndy);
                    break;

                default:
                    way = BrLine(mStartx, mStarty, mEndx, mEndy);
                    break;
            }

            ShowWay(way);
        }

        private void ShowWay(List<(int x, int y)> aWay)
        {
            Graphics g = Graphics.FromImage(mBitmap);
            Brush brush = new SolidBrush(Color.Brown);
            Pen p = new Pen(brush);
            int st = 4;
            int ht = st / 2;

            //List<(int x, int y)> wa = new List<(int x, int y)>();
            //for (int ii = 0; ii < aWay.Count; ii++)
            //{
            //    if (!wa.Contains(aWay[ii]))
            //        wa.Add(aWay[ii]);
            //}
            //aWay = wa;

            if (tbAlg.Text == "0")
            {
                aWay.Add((mStartx, mStarty));
                aWay.Reverse();
            }

            if (!aWay.Contains((mEndx, mEndy)))
                aWay.Add((mEndx, mEndy));

            for (int i = 1; i < aWay.Count; i++)
            {
                if (cbPoints.Checked)
                    g.FillRectangle(brush, aWay[i].x * mCellW + mCellW / 2, aWay[i].y * mCellW + mCellW / 2, st, st);
                if (cbLines.Checked)
                    g.DrawLine(p, aWay[i].x * mCellW + mCellW / 2, aWay[i].y * mCellW + mCellW / 2, aWay[i - 1].x * mCellW + mCellW / 2, aWay[i - 1].y * mCellW + mCellW / 2);
            }

            panel1.Refresh();
        }

        private List<(int x, int y)> DirectWay(int aBx, int aBy, int aEx, int aEy)
        {
            List<(int x, int y)> wad = new List<(int x, int y)>();
            int dx = aBx - aEx;
            int dy = aBy - aEy;

            if (Math.Abs(dx) < Math.Abs(dy))
                MakeWay(Math.Abs(dx), Math.Abs(dy), Math.Abs(dy) - Math.Abs(dx), false);
            else
                MakeWay(Math.Abs(dy), Math.Abs(dx), Math.Abs(dx) - Math.Abs(dy), true);

            void MakeWay(int mind, int maxd, int ad, bool onX)
            {
                bool c = (mind == 0) || (maxd == 0);

                if (c)
                    ad = maxd;

                int xx = -1;
                int yy = -1;

                if (dx < 0)
                    xx = 1;
                if (dy < 0)
                    yy = 1;

                for (int l = 1; l <= ad; l++)
                {
                    if (onX)
                        wad.Add((aBx + l * xx, aBy));
                    else
                        wad.Add((aBx, aBy + l * yy));
                }

                if (c)
                {
                    wad.RemoveAt(wad.Count - 1);
                    return;
                }

                for (int i = 1; i < mind; i++)
                {
                    if (wad[wad.Count - 1].x + 1 * xx == aEx && wad[wad.Count - 1].y + 1 * yy == aEy)
                        break;

                    wad.Add((wad[wad.Count - 1].x + 1 * xx, wad[wad.Count - 1].y + 1 * yy));
                }
            }

            return wad;
        }

        private List<(int x, int y)> DirectWay2(int aBx, int aBy, int aEx, int aEy)
        {
            List<(int x, int y)> wad = new List<(int x, int y)>();

            int dx = aBx - aEx;
            int dy = aBy - aEy;

            int x1;
            int x2;
            int y1;
            int y2;

            if (aBx > aEx)
            {
                x1 = aEx;
                x2 = aBx;
                y1 = aEy;
                y2 = aBy;
            }
            else
            {
                x1 = aBx;
                x2 = aEx;
                y1 = aBy;
                y2 = aEy;
            }

            for (int x = x1; x <= x2; x++)
            {
                int y = y1 + dy * (x - x1) / dx;
                wad.Add((x, y));
            }

            return wad;
        }

        private List<(int x, int y)> DirectWay3(int aBx, int aBy, int aEx, int aEy)
        {
            List<(int x, int y)> wad = new List<(int x, int y)>();
            wad.Add((aBx, aBy));

            int dx = aBx - aEx;
            int dy = aBy - aEy;
            double mdx = Math.Abs(dx);
            double mdy = Math.Abs(dy);

            int xx = (dx < 0) ? 1 : -1;
            int yy = (dy < 0) ? 1 : -1;

            if (dx == 0 || dy == 0 || dx == dy)
            {
                wad = DirectWay(aBx, aBy, aEx, aEy);
                return wad;
            }

            double p = (mdx < mdy) ? mdx : mdy;
            double ost;
            bool dpx;

            if (mdx == p)
            {
                ost = mdy - p;
                dpx = false;
            }
            else
            {
                ost = mdx - p;
                dpx = true;
            }

            int ii = (dpx) ? Math.Abs(dx) : Math.Abs(dy);
            double k = p / (ost + p);
            double kk = p / (ost + p);

            for (int i = 1; i < ii; i++)
            {
                if (k >= 0.98)
                {
                    k -= 1;

                    Step(true);
                }
                else
                    Step(false);

                k += kk;
            }

            return wad;

            void Step(bool diag)
            {
                if (diag)
                {
                    wad.Add((wad[wad.Count - 1].x + 1 * xx, wad[wad.Count - 1].y + 1 * yy));
                    return;
                }

                if (dpx)
                    wad.Add((wad[wad.Count - 1].x + 1 * xx, wad[wad.Count - 1].y));
                else
                    wad.Add((wad[wad.Count - 1].x, wad[wad.Count - 1].y + 1 * yy));
            }
        }

        private List<(int x, int y)> aStar(int aBx, int aBy, int aEx, int aEy)
        {
            List<(int x, int y)> wad = new List<(int x, int y)>();
            List<(int x, int y)> waid = new List<(int x, int y)>();
            List<(int x, int y)> dw = new List<(int x, int y)>();
            wad = BrLine(aBx, aBy, aEx, aEy);

            if (wad.Count < 2)
                return wad;

            int i;
            int d;

            for (int m = 0; m < wad.Count; m++)
            {
                int v = mMap[wad[m].x][wad[m].y];

                if (v == 800 || v == 1200 || v == -1)
                {
                    if (m < 2)
                        m = 2;

                    waid.Add((wad[m - 2].x, wad[m - 2].y));
                    dw.Add((wad[m - 1].x - wad[m - 2].x, wad[m - 1].y - wad[m - 2].y));
                    wad.RemoveRange(m - 1, wad.Count - m + 1);
                    break;
                }
            }

            if (wad.Contains((aEx, aEy)))
                return wad;

            List<(int x, int y)> direc = new List<(int x, int y)>
            {
                (0, 1),
                (-1, 1),
                (-1, 0),
                (-1, -1),
                (0, -1),
                (1, -1),
                (1, 0),
                (1, 1),
            };
            bool next = false;

            for (i = 0; i < waid.Count; i++)
            {
                int dir = direc.IndexOf((dw[i].x, dw[i].y));

                for (d = 0; d < direc.Count; d++)
                {
                    Point p = new Point(waid[i].x + direc[dir].x, waid[i].y + direc[dir].y);
                    dir++;

                    if (dir == direc.Count)
                        dir = 0;

                    int v = mMap[p.X + 1][p.Y + 1];

                    if (v != 800 && v != 1200 && v != -1)
                    {
                        CheckAndAdd(p.X, p.Y);

                        for (int s = 0; s < 8; s++)
                        {
                            if (wad.Contains((aEx + direc[s].x, aEy + direc[s].y)))
                                return wad;
                        }

                        if (next)
                        {
                            next = false;
                            break;
                        }
                    }
                }
            }

            void CheckAndAdd(int x, int y)
            {
                if (wad.Contains((x, y)))
                    return;

                waid.Add((x, y));
                wad.Add((x, y));
                next = true;

                TryToGoBack(x, y);
                TryToGo(x, y);

                void TryToGo(int ax, int ay)
                {
                    List<(int x, int y)> twad = BrLine(ax, ay, aEx, aEy);

                    for (int s = 1; s < twad.Count; s++)
                    {
                        int v = mMap[twad[s].x + 1][twad[s].y + 1];

                        if (v == 1200 || v == 800)
                        {
                            if (s < 4)
                            {
                                dw.Add((twad[1].x - twad[0].x, twad[1].y - twad[0].y));
                                return;
                            }

                            twad.RemoveRange(s - 1, twad.Count - s + 1);
                            break;
                        }
                    }

                    for (int s = 1; s < twad.Count; s++)
                    {
                         wad.Add(twad[s]);
                    }

                    if (wad[wad.Count - 1] == (aEx, aEy))
                        return;

                    twad = aStar(wad[wad.Count - 1].x, wad[wad.Count - 1].y, aEx, aEy);

                    for (int s = 1; s < twad.Count; s++)
                    {
                            wad.Add(twad[s]);
                    }
                }
                void TryToGoBack(int ax, int ay)
                {
                    var trw = BrLine(aBx, aBy, ax, ay);

                    for (int m = 0; m < trw.Count; m++)
                    {
                        int v = mMap[trw[m].x + 1][trw[m].y + 1];

                        if (v == 800 || v == 1200 || v == -1)
                            return;
                    }

                    if (trw.Count > 1 && trw.Count < wad.Count)
                    {
                        waid.Clear();
                        wad.Clear();
                        for (int s = 0; s < trw.Count; s++)
                        {
                            waid.Add(trw[s]);
                            wad.Add(trw[s]);
                        }
                        i = waid.Count - 2;
                    }
                }
            }

            return wad;
        }

        private List<(int x, int y)> BrLine(int aBx, int aBy, int aEx, int aEy)
        {
            List<(int x, int y)> wad = new List<(int x, int y)>();

            bool swap = Math.Abs(aEx - aBx) < Math.Abs(aEy - aBy);
            if (swap)
                (aBx, aBy, aEx, aEy) = (aBy, aBx, aEy, aEx);
            if (aBx > aEx)
                (aBx, aEx, aBy, aEy) = (aEx, aBx, aEy, aBy);

            int dx = aEx - aBx;
            int dy = aEy - aBy;

            int iy = dy < 0 ? -1 : 1;
            if (iy < 0)
                dy = -dy;

            int D = (2 * dy) - dx;
            int y = aBy;
            for (int x = aBx; x <= aEx; x++)
            {
                wad.Add(swap ? (y, x) : (x, y));
                if (D > 0)
                {
                    y += iy;
                    D = D + (2 * (dy - dx));
                }
                else
                {
                    D = D + 2 * dy;
                }
            }

            return wad;
        }

        private List<(int x, int y)> ShortWaySan(int aBx, int aBy, int aEx, int aEy)
        {
            aBx++; aBy++; aEx++; aEy++;

            List<List<int>> rooms = new List<List<int>>();

            for (int x = 0; x < mMapW; x++)
            {
                rooms.Add(new List<int>());
                for (int y = 0; y < mMapH; y++)
                    rooms[x].Add((-1));
            }
            rooms[aBx][aBy] = mMap[aBx][aBy];

            List<List<(int x, int y)>> ways = new List<List<(int, int)>>();
            ways.Add(new List<(int x, int y)>());
            ways[0].Add((aBx, aBy));

            List<(int x, int y)> roomq = new List<(int, int)>();
            //List<(int x, int y)> way = new List<(int, int)>();
            roomq.Add((aBx, aBy));

            while (roomq.Count > 0)
            {
                (int x, int y) = roomq[0];
                roomq.RemoveAt(0);
                //way.Add((x, y));

                int price = rooms[x][y];
                int nextways = 0;
                int value;
                int waynum = 0;

                foreach (var elem in ways)
                {
                    if (elem.Contains((x, y)))
                        waynum = ways.IndexOf(elem);
                }

                //if (rooms[aEx][aEy] != -1 && rooms[aEx][aEy] <= price)
                //{
                //    if (x == aEx && y == aEy)
                //        break;

                //    ways.RemoveAt(waynum);
                //    continue;
                //}

                if (x < mMapW - 1)
                    CheckDirection(1, 0);
                if (y < mMapH - 1 && x < mMapW - 1)
                    CheckDirection(1, 1);
                if (y < mMapH - 1)
                    CheckDirection(0, 1);
                if (x > 1 && y < mMapH - 1)
                    CheckDirection(-1, 1);
                if (x > 1)
                    CheckDirection(-1, 0);
                if (x > 1 && y > 1)
                    CheckDirection(-1, -1);
                if (y > 1)
                    CheckDirection(0, -1);
                if (x < mMapW - 1 && y > 1)
                    CheckDirection(1, -1);

                while (ways[ways.Count - 1].Count == 0)
                    ways.RemoveAt(ways.Count - 1);

                if (nextways == 0)
                    ways.RemoveAt(waynum);

                void CheckDirection(int px, int py)
                {
                    if (roomq.Count == 0 && x != aBx && y != aBy)
                        return;

                    int ax = x + px;
                    int ay = y + py;
                    int sum = rooms[ax][ay];
                    value = mMap[ax][ay] + price;

                    if (sum != -1 && sum <= value)
                        return;

                    if (sum != -1)
                    {
                        for (int i = 0; i < ways.Count; i++)
                            if (ways[i].Contains((ax, ay)))
                            {
                                waynum = i < waynum ? waynum - 1 : waynum;
                                ways.RemoveAt(i);
                                break;
                            }
                    }
                    else
                    {
                        int i = FindQueuePlace(ax, ay, value);
                        roomq.Insert(i, (ax, ay));
                    }

                    rooms[ax][ay] = value;

                    if (nextways != 0)
                    {
                        int n = ways.Count - 1;

                        for (int i = 0; i < ways[waynum].Count - 1; i++)
                            ways[n].Add((ways[waynum][i]));

                        ways[n].Add((ax, ay));
                    }
                    else
                    {
                        ways[waynum].Add((ax, ay));
                    }

                    if ((ax, ay) == (aEx, aEy))
                    {
                        roomq.Clear();
                        nextways = 1;
                        return;
                    }

                    ways.Add(new List<(int x, int y)>()); 
                    nextways++;
                }
            }

            //return way;
            List<(int x, int y)> mway = null;

            for (int i = ways.Count - 1; i > -1; i--)
                if ((ways[i][ways[i].Count - 1]) == ((aEx, aEy)))
                {
                    mway = ways[i];
                    break;
                }

            for (int i = 0; i < mway.Count; i++)
                mway[i] = (mway[i].x - 1, mway[i].y - 1);

            return mway;

            int FindQueuePlace(int ax, int ay, int cost)
            {
                int i = 0;
                while (i < roomq.Count && rooms[roomq[i].x][roomq[i].y] < cost)
                    i++;

                return i;
            }
        }

        private List<(int x, int y)> ShortWay0_1(int aBx, int aBy, int aEx, int aEy)
        {
            aBx++; aBy++; aEx++; aEy++;
            int hp = mMapH + 2;
            int wp = mMapW + 2;

            List<(int x, int y)> answer = new List<(int, int)>();
            List<List<int>> rooms01 = new List<List<int>>();
            List<int> rooms = new List<int>();

            CreateMap0_1(rooms01);
            for (int i = 0; i < wp * hp; i++)
            {
                rooms.Add((0));
            }

            List<(int x, int y, int f)> roomq = new List<(int, int, int)>();
            List<List<(int x, int y)>> ways = new List<List<(int, int)>>();

            ways.Add(new List<(int x, int y)>());
            ways[0].Add((aBx, aBy));
            roomq.Add((aBx, aBy, 0));
            rooms[aBx + aBy * hp] = 0;

            bool end = false;
            //int bestg = 10000;
            //g = path sum
            while (roomq.Count > 0)
            {
                (int x, int y, int f) = roomq[0];
                roomq.RemoveAt(0);

                int cg = rooms[x + y * wp];
                int nextways = -1;
                int waynum = 0;

                for (int i = 1; i < ways.Count; i++)
                    if (ways[i].Contains((x, y)))
                    {
                        waynum = i;
                        break;
                    }

                CheckSide(1, 0);
                CheckSide(1, 1);
                CheckSide(0, 1);
                CheckSide(-1, 1);
                CheckSide(-1, 0);
                CheckSide(-1, -1);
                CheckSide(0, -1);
                CheckSide(1, -1);

                if (nextways > -1)
                    ways.RemoveAt(ways.Count - 1);
                if (nextways == -1)
                {
                    ways.RemoveAt(waynum);
                }

                void CheckSide(int px, int py) //plus x, plus y
                {
                    if (end)
                        return;

                    px += x;
                    py += y;

                    if (rooms01[py][px] == 0)
                        return;

                    int nextg = rooms[px + py * wp];

                    if (nextg != 0 && cg + 1>= nextg)
                        return;

                    if (nextg != 0)
                    {
                        for (int i = 0; i < ways.Count; i++)
                        {
                            if (ways[i].Contains((px, py)))
                            {
                                waynum = i < waynum ? waynum - 1 : waynum;
                                ways.RemoveAt(i);
                                int idx = roomq.FindIndex(elem => (elem.x == px && elem.y == py));

                                if (idx >= 0)
                                    roomq.RemoveAt(idx);
                            }
                        }
                    }

                    (int place, int nf) = PutInQueue(px, py, heuristic(px, py), cg + 1);
                    roomq.Insert(place, (px, py, nf));
                    rooms[px + py * wp] = cg + 1;

                    int n = waynum;

                    if (nextways == -1)
                    {
                        ways[n].Add((px, py));
                    }
                    else
                    {
                        n = ways.Count - 1;

                        for (int i = 0; i < ways[waynum].Count - 1; i++)
                            ways[n].Add((ways[waynum][i]));

                        ways[n].Add((px, py));
                    }

                    if (px == aEx && py == aEy)
                    {
                        roomq.RemoveAt(0);
                        end = true;
                    }

                    nextways++;
                    List<(int x, int y)> l = new List<(int x, int y)>();
                    ways.Add(l);
                }
            }

            for (int i = 0; i < ways[0].Count; i++)
            {
                answer.Add((ways[0][i].x - 1, ways[0][i].y - 1));
            }

            answer.Add((aEx - 1, aEy - 1));

            return answer;

            (int idx, int f) PutInQueue(int ax, int ay, int ah, int ag)
            {
                int f = ah + 10 * ag; //10 - коэфициент
                int i = 0;

                while (i < roomq.Count && f >= roomq[i].f)
                    i++;

                return (i, f);
            }

            int heuristic(int x, int y)
            {
                int dx = Math.Abs(x - aEx);
                int dy = Math.Abs(y - aEy);
                int min = Math.Min(dx, dy);

                //int rounder;
                //int diag = Math.DivRem(14 * m, 10, out rounder);
                //diag = rounder == 0 ? diag : diag + 1;
                //int h = (dx + dy) + diag - 2 * m;

                int h = 14 * min + 10 * ((dx + dy - 2 * min));

                return h;
            }
        }

        private List<(int x, int y)> WikiAstar(int aBx, int aBy, int aEx, int aEy)
        {
            aBx++; aBy++; aEx++; aEy++;
            int hp = mMapH + 2;
            int wp = mMapW + 2;

            List<List<int>> rooms01 = new List<List<int>>();
            List<(int x, int y)> total_path = new List<(int x, int y)>();
            List<(int x, int y)> OpenSet = new List<(int x, int y)>();

            (int y, int x)[,] CameFrom = new (int y, int x)[hp, wp];
            int[] gScore = Enumerable.Repeat(Int32.MaxValue, hp * wp).ToArray(); // TODO connect with OpenSet
            int[] fScore = Enumerable.Repeat(Int32.MaxValue, hp * wp).ToArray(); // 

            fScore[aBx + aBy * wp] = heuristic(aBx, aBy);
            gScore[aBx + aBy * wp] = 0;
            CreateMap0_1(rooms01);
            OpenSet.Add((aBx, aBy));

            while (OpenSet.Count > 0)
            {
                (int cx, int cy) = BestInQueue();

                if (cx == aEx && cy == aEy)
                {
                    construct_the_way(cx, cy);
                    break;
                }

                CheckSide(1, 0, false);
                CheckSide(1, 1, true);
                CheckSide(0, 1, false);
                CheckSide(-1, 1, true);
                CheckSide(-1, 0, false);
                CheckSide(-1, -1, true);
                CheckSide(0, -1, false);
                CheckSide(1, -1, true);

                void CheckSide(int dx, int dy, bool diag)
                {
                    int nx = cx + dx;
                    int ny = cy + dy;
                    int point = nx + ny * wp;

                    if (rooms01[ny][nx] == 0)
                        return;

                    int next_gscore = diag ? gScore[cx + cy * wp] + 14 : gScore[cx + cy * wp] + 10;

                    if (next_gscore < gScore[point])
                    {
                        fScore[point] = next_gscore + heuristic(nx, ny);
                        gScore[point] = next_gscore;
                        CameFrom[ny, nx] = (cx, cy);

                        if (!OpenSet.Contains((nx, ny)))
                            OpenSet.Add((nx, ny));
                    }
                }
            }

            void construct_the_way(int ex, int ey)
            {
                while (true)
                {
                    total_path.Add((ex - 1, ey - 1));

                    if ((ex, ey) == (aBx, aBy))
                        break;

                    (ex, ey) = CameFrom[ey, ex];
                }

                total_path.Reverse();
            }

            return total_path;

            (int bx, int by) BestInQueue()
            {
                (int x, int y) = OpenSet[0];
                (int bx, int by) = (x, y);
                int bf = fScore[x + y * wp];
                int bidx = 0;

                for (int i = 0; i < OpenSet.Count; i++)
                {
                    if (bf > fScore[x + y * wp])
                    {
                        (bx, by) = (x, y);
                        bidx = i - 1;
                        bf = fScore[x + y * wp];
                    }

                    (x, y) = OpenSet[i];
                }

                OpenSet.RemoveAt(bidx);
                return (bx, by);
            }

            int heuristic(int x, int y)
            {
                int dx = Math.Abs(x - aEx);
                int dy = Math.Abs(y - aEy);
                int min = Math.Min(dx, dy);

                int h = 14 * min + 10 * ((dx + dy - 2 * min));

                return h;
            }
        }

        #region DimWay
        private List<(int x, int y)> ShortWayDim(int aBx, int aBy, int aEx, int aEy)
        {
            aBx++; aBy++; aEx++; aEy++;
            List<List<int>> map = new List<List<int>>();
            CreateMap(map, mMapW, mMapH);

            List<(int x, int y, int summ)> ways = new List<(int, int, int)>();
            int bestSumm = int.MaxValue;
            map[aBx][aBy] = 1;
            ways.Add((aBx, aBy, 0));

            //List<(int x, int y)> way = new List<(int x, int y)>(); 
            //map[aEx][aEy] = -1;
            //ways.Add((aBx, aBy, 0));

            while (ways.Count > 0)
            {
                int idx = FindMinSumm();

                (int x, int y, int summ) = ways[idx];
                ways.RemoveAt(idx);
                //way.Add((x, y));

                if (AddToList(x - 1, y - 1, summ, true))
                    break;
                if (AddToList(x + 1, y - 1, summ, true))
                    break;
                if (AddToList(x + 1, y + 1, summ, true))
                    break;
                if (AddToList(x - 1, y + 1, summ, true))
                    break;
                if (AddToList(x + 0, y - 1, summ, false))
                    break;
                if (AddToList(x + 0, y + 1, summ, false))
                    break;
                if (AddToList(x - 1, y - 0, summ, false))
                    break;
                if (AddToList(x + 1, y - 0, summ, false))
                    break;
            }

            //return way;

            if (cbWayPrice.Checked)
                ShowCurState();

            return CalckBackWay();

            bool AddToList(int aX, int aY, int aSumm, bool aDiag)
            {
                if (aX == aEx && aY == aEy)
                {
                    if (aSumm < bestSumm)
                        bestSumm = aSumm;
                    rtNewColors.Text += "" + bestSumm;
                    return true;
                }

                if (mMap[aX][aY] < 0)
                    return false;
                int add = mMap[aX][aY]; //int add = aDiag ? mMap[aX][aY] * 14 / 10 : mMap[aX][aY];
                int nSumm = aSumm + add;
                if (map[aX][aY] != 0 && map[aX][aY] <= nSumm)
                    return false;
                map[aX][aY] = nSumm;
                ways.Add((aX, aY, nSumm));
                return false;
            }

            void ShowCurState()
            {
                Graphics g = Graphics.FromImage(mBitmap);
                Brush brush = new SolidBrush(Color.Magenta);

                for (int x = 1; x < mMapW; x++)
                    for (int y = 1; y < mMapH; y++)
                    {
                        int v = map[x][y];
                        if (v > 0)
                            g.DrawString("" + v / 100, Font, Brushes.Red, x * mCellW, y * mCellW);
                    }
                panel1.Refresh();
            }

            int FindMinSumm()
            {
                int idx = 0;
                int mins = ways[0].summ;
                for (int i = 1; i < ways.Count; ++i)
                    if (ways[i].summ < mins)
                    {
                        mins = ways[i].summ;
                        idx = i;
                    }
                return idx;
            }

            List<(int x, int y)> CalckBackWay()
            {
                var _way = new List<(int x, int y)>();
                int curX = aEx; int curY = aEy;
                int cmin = int.MaxValue; int nx = 0; int ny = 0;
                while (true)
                {
                    CheckW(-1, 0);
                    CheckW(+1, 0);
                    CheckW(0, -1);
                    CheckW(0, +1);
                    CheckW(-1, -1);
                    CheckW(+1, -1);
                    CheckW(-1, +1);
                    CheckW(+1, +1);

                    if (nx == aBx && ny == aBy)
                        break;
                    _way.Add((nx - 1, ny - 1));
                    curX = nx; curY = ny;
                }

                return _way;

                void CheckW(int aDx, int aDy)
                {
                    int v = map[curX + aDx][curY + aDy];
                    if (v <= 0)
                        return;
                    if (v >= cmin)
                        return;
                    nx = curX + aDx;
                    ny = curY + aDy;
                    cmin = v;
                }
            }
        }

        #endregion
    }
}