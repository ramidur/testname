﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Domino
{
    public partial class Form1 : Form
    {
        List<Domino> Items = new List<Domino>();
        List<int> Result = new List<int>();
        public Form1()
        {
            InitializeComponent();

            Items.Add(new Domino(7, 8));
            Items.Add(new Domino(8, 8));
            Items.Add(new Domino(2, 6));
            Items.Add(new Domino(3, 2));
            Items.Add(new Domino(4, 1));
            Items.Add(new Domino(5, 3));
            Items.Add(new Domino(2, 4));
            Items.Add(new Domino(1, 2));
            Items.Add(new Domino(0, 4));
            Items.Add(new Domino(3, 3));
            Items.Add(new Domino(4, 1));
            Items.Add(new Domino(1, 1)); 
            Items.Add(new Domino(5, 0));
            Items.Add(new Domino(2, 6));
            Items.Add(new Domino(3, 2));
            Items.Add(new Domino(4, 1));
            Items.Add(new Domino(5, 3));
            Items.Add(new Domino(2, 4));
            Items.Add(new Domino(3, 2));
            Items.Add(new Domino(5, 0));
            //Items.Add(new Domino(4, 1));
            //Items.Add(new Domino(5, 3));
            //Items.Add(new Domino(2, 4));
            label2.Text = "" + Factorial(Items.Count);

            int j = 0;
            foreach (var elem in Items)
                richTextBox1.Text += (j++) + ": " + elem.p1 + " " + elem.p2 + "\n";

        }

        public long Factorial(int f)
        {
            if (f == 0)
                return 1;
            else
                return f * Factorial(f - 1);
        }

        int CalckCount = 0;
        int percent = 0;
        private void Calck(int aLast, List<int> aUsed)
        {
            CalckCount++;
            if (CalckCount%11111==0)
            {
                label2.Text = "" + percent + "%";
                ShowCurState();
                Application.DoEvents();
            }

            List<int> nu = new List<int>(aUsed);
            for (int i=0; i< Items.Count; ++i)
            {
                if (aUsed.Contains(i))
                    continue;
                bool added = false;
                if (aLast==-1 || Items[i].p1 == aLast)
                {
                    added = true;
                    nu.Add(i);
                    Calck(Items[i].p2, nu);
                }

                if (aLast == -1 || Items[i].p2 == aLast)
                {
                    if (!added)
                    {
                        added = true;
                        nu.Add(i);
                    }

                    Calck(Items[i].p1, nu);
                }

                if (added)
                {
                    if (Result.Count < nu.Count)
                        Result = new List<int>(nu);
                    nu.Remove(i);

                    if (Result.Count == Items.Count)
                        break;
                }

                if (aLast == -1)
                    percent = (i + 1) * 100 / Items.Count;
            }
        }

        private void ShowCurState()
        {
            label1.Text = CalckCount.ToString("#,##0");
            string s = "";
            foreach (var i in Result)
                s += i + ": " + Items[i].p1 + " " + Items[i].p2 + "\n";
            richTextBox2.Text = s;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            if (CalckCount != 0)
                return;

            RemoveAndSort();
            Calck(-1, new List<int>());

            ShowCurState();
        }

        private void RemoveAndSort()
        {
            //sort leftsmall rightbig;
            //remove worked doubles (worked - have anathers bone)
            //remove worked equals

            //calk nums count
            //remove 2 multytimes
        }
    }


    struct Domino
    {
        public int p1;
        public int p2;

        public Domino(int a1, int a2)
        {
            p1 = a1;
            p2 = a2;
        }

    };
}
