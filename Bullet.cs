﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TowerDef
{
    public abstract class Bullet : GameObject
    {
        protected int mDamage;
        protected double mspeed;
        protected int mspeedX, mspeedY;
        public Control mControl;
        protected GameField mGameField;
        protected int mWidth, mHeight;
        protected Color mColor;
        protected double mCurAngle;

        public override void SelfDelete()
        {
            mControl.Dispose();
        }

        public Point GetLoc() { return mControl.Location; }

        public Size GetSize() { return mControl.Size; }

        public void Init(GameField aGameField, Point aSpawnPoint)
        {
            mGameField = aGameField;
            InitControl(aSpawnPoint);
        }

        protected virtual void InitControl(Point aSpawnPoint)
        {
            Button bt = new Button();
            mControl = bt;
            mGameField.mField.Controls.Add(mControl);
            mControl.Size = new Size(mWidth, mHeight);
            mControl.BackColor = mColor;
            mControl.Location = aSpawnPoint;
            mControl.BringToFront();
        }

        public override void Step()
        {
            CheckPosition();

            mControl.Left += mspeedX;
            mControl.Top += mspeedY;

            CheckColl();
        }

        public virtual void CheckColl()
        {
            foreach (var elem in mGameField.Zombies)
            {
                if (Gutils.CheckCollision(elem, this))
                {
                    Damaged(1, elem);
                }
            }
        }

        public virtual void Damaged(int adamage, Zombie atarget)
        {
            hp -= adamage;

            if (hp <= 0)
                isdeleted = true;

            if (!atarget.CantBeHit)
                atarget.Damaged(mDamage);
        }

        public virtual void SetVector(double angle)
        {
            mCurAngle = angle;
            ChangeSpeed();
        }

        public void ChangeSpeed()
        {
            mspeedX = Convert.ToInt32(mspeed * Math.Cos(mCurAngle));
            mspeedY = Convert.ToInt32(mspeed * Math.Sin(mCurAngle));
        }

        public virtual double GetSpeed()
        {
            return mspeed;
        }

        protected virtual void CheckPosition()
        {
            if (GetLoc().Y < 0 || GetLoc().Y > 600)
                isdeleted = true;
            if (GetLoc().X > mGameField.mField.Size.Width || GetLoc().X < 0)
                isdeleted = true;
        }
    }

    public class BulletSimple : Bullet
    {
        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.DarkGray;
            mWidth = 15;
            mHeight = 15;
            mspeed = 15;
            mDamage = 15;
            hp = 1;

            base.InitControl(aSpawnPoint);
        }
    }

    public class BulletHeavy : Bullet
    {
        int pierce = 0;

        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.DarkBlue;
            mWidth = 15;
            mHeight = 15;
            mspeed = 30;
            mDamage = 25;
            hp = 3;

            base.InitControl(aSpawnPoint);
        }

        public override void CheckColl()
        {
            if (pierce <= 0)
                base.CheckColl();
            else
                pierce--;
        }

        public override void Damaged(int adamage, Zombie atarget)
        {
            base.Damaged(adamage, atarget);

            pierce = 2;
        }
    }

    public class BulletBoomerang : Bullet
    {
        private int mFreeFlyTime = 15;

        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.RoyalBlue;
            mWidth = 20;
            mHeight = 20;
            mspeed = 25;
            mDamage = 10;
            hp = 10;

            base.InitControl(aSpawnPoint);
        }

        public override void Step()
        {
            if (mGameField.Zombies.Count != 0 && mFreeFlyTime <= 0)
            {
                Zombie azb = Gutils.FindRandomZombie(mGameField);
                double angle = Gutils.FindAngleTarget(mspeed, azb.GetSpeedX(), azb.GetSpeedY(), azb.GetLoc().X + azb.GetSize().Width / 2 - GetLoc().X, azb.GetLoc().Y + azb.GetSize().Height / 2 - GetLoc().Y);
                SetVector(angle);
                mFreeFlyTime = 16;
            }

            mFreeFlyTime--;

            base.Step();
        }
    }

    public class BulletFreeze : Bullet
    {
        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.Azure;
            mWidth = 15;
            mHeight = 15;
            mspeed = 15;
            mDamage = 15;
            hp = 1;

            base.InitControl(aSpawnPoint);
        }

        public override void Damaged(int adamage, Zombie atarget)
        {
            base.Damaged(adamage, atarget);

            if (atarget.mEffects.OfType<Glacial>().Any())
                return;

            DurationEffect frz = new Frozen();
            atarget.TakeEffect(frz);
        }
    }

    public class BulletCryo : Bullet
    {
        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.DeepSkyBlue;
            mWidth = 17;
            mHeight = 17;
            mspeed = 12;
            mDamage = 0;
            hp = 1;

            base.InitControl(aSpawnPoint);
        }

        public override void Damaged(int adamage, Zombie atarget)
        {
            if (atarget.GetType() == typeof(ZombieBoss) || atarget.mEffects.OfType<Frozen>().Any())
                return;

            base.Damaged(adamage, atarget);

            DurationEffect frz = new Glacial();
            atarget.TakeEffect(frz);
        }
    }

    public class BulletCrystal : Bullet
    {
        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.Azure;
            mWidth = 20;
            mHeight = 20;
            mspeed = 25;
            mDamage = 20;
            hp = 1;

            base.InitControl(aSpawnPoint);
        }

        public override void Damaged(int adamage, Zombie atarget)
        {
            base.Damaged(adamage, atarget);

            DurationEffect frz = new Crystalized();
            atarget.TakeEffect(frz);

            Shatter();
        }

        private void Shatter()
        {
            Random rnd = new Random();

            int colvo = Gutils.rnd.Next(4, 7);

            for (int i = 0; i < colvo; i++)
            {
                Bullet bull = (Bullet)Activator.CreateInstance(mGameField.BulletTypes[6]);
                bull.Init(mGameField, mControl.Location);

                double d = rnd.NextDouble() * Math.PI * rnd.Next(-2, 1);
                bull.SetVector(d);
                mGameField.AddGobject(bull);
            }
        }
    }

    public class ShardCrystal : Bullet
    {
        int mtimer = 20;

        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.Azure;
            mWidth = 5;
            mHeight = 5;
            mspeed = 10;
            mDamage = 5;
            hp = 1;

            base.InitControl(aSpawnPoint);
        }

        public override void Step()
        {
            base.Step();

            mtimer--;

            if (mtimer <= 0)
                isdeleted = true;
        }

        public override void CheckColl()
        {
            if (mtimer > 17)
                return;

            base.CheckColl();
        }
    }

    public class BulletFireBall : Bullet
    {
        int acceleration = 2;
        int Vo = 8;

        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.Firebrick;
            mWidth = 15;
            mHeight = 15;
            mspeed = Vo;
            mDamage = 15;
            hp = 1;

            base.InitControl(aSpawnPoint);
        }

        public override void Step()
        {
            base.Step();

            mspeed += acceleration;
            ChangeSpeed();
        }

        public override void Damaged(int adamage, Zombie atarget)
        {
            base.Damaged(adamage, atarget);

            atarget.mEffects.Clear();

            DurationEffect frz = new Burning();
            atarget.TakeEffect(frz);
        }

        public override double GetSpeed()
        {
            return (Vo);
        }
    }

    public class BulletRocket : Bullet
    {
        int explRadius = 125;

        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.SteelBlue;
            mWidth = 25;
            mHeight = 25;
            mspeed = 15;
            mDamage = 30;
            hp = 1;
            
            base.InitControl(aSpawnPoint);
        }

        public override void Step()
        {
            base.Step();

            if (mGameField.Zombies.Count == 0)
                return;

            mCurAngle = Gutils.PointsToAngle(GetLoc(), mGameField.Zombies[mGameField.Zombies.Count - 1].GetLoc());

            ChangeSpeed();
        }

        public override void Damaged(int adamage, Zombie atarget)
        {
            isdeleted = true;
            Random rnd = new Random();

            for (int i = 0; i < 10; i++)
            {
                Bullet bull = (Bullet)Activator.CreateInstance(mGameField.BulletTypes[9]);
                bull.Init(mGameField, mControl.Location);

                double d = rnd.NextDouble() * Math.PI * rnd.Next(-3, 2);
                bull.SetVector(d);
                mGameField.AddGobject(bull);
            }

            foreach (var elem in mGameField.Zombies)
            {
                double distance = Gutils.FindDist(elem.GetLoc(), GetLoc());

                if (distance > explRadius)
                    continue;

                elem.Damaged(mDamage);
                elem.mEffects.Clear();
                DurationEffect frz = new Burning();
                elem.TakeEffect(frz);
            }
        }
    }

    public class ExplosionParticles : Bullet
    {
        int mtimer = 10;

        protected override void InitControl(Point aSpawnPoint)
        {
            mColor = Color.OrangeRed;
            mWidth = 20;
            mHeight = 20;
            mspeed = 5;
            mDamage = 0;
            hp = 1;

            base.InitControl(aSpawnPoint);
        }

        public override void Step()
        {
            base.Step();

            mtimer--;

            if (mtimer <= 0)
                isdeleted = true;
        }

        public override void CheckColl()
        {
            return;
        }
    }
}