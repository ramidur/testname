﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TowerDef
{
    public partial class Form1 : Form
    {
        GameField mGameField;

        public Form1()
        {
            InitializeComponent();
            mGameField = new GameField(pn_field, timer1);

            mGameField.pathbts.Add(bt_path0);
            mGameField.pathbts.Add(bt_path1);
            mGameField.pathbts.Add(bt_path2);
            mGameField.pathbts.Add(bt_path3);
            mGameField.pathbts.Add(bt_path4);
            mGameField.pathbts.Add(bt_path5);
            mGameField.pathbts.Add(bt_path6);
            mGameField.pathbts.Add(bt_path7);

            mGameField.SenPlaces.Add(Sen0);
            mGameField.SenPlaces.Add(Sen1);
            mGameField.SenPlaces.Add(Sen2);
            mGameField.SenPlaces.Add(Sen3);
            mGameField.SenPlaces.Add(Sen4);
            mGameField.SenPlaces.Add(Sen5);
            mGameField.SenPlaces.Add(Sen6);

            mGameField.ChoosBttns.Add(bt_SenType0);
            mGameField.ChoosBttns.Add(bt_SenType1);
            mGameField.ChoosBttns.Add(bt_SenType2);
            mGameField.ChoosBttns.Add(bt_SenType3);
            mGameField.ChoosBttns.Add(bt_SenType4);
            mGameField.ChoosBttns.Add(bt_SenType5);
            mGameField.ChoosBttns.Add(bt_SenType6);
            mGameField.ChoosBttns.Add(bt_SenType7);
            mGameField.ChoosBttns.Add(bt_SenType8);

            //TODO записать в файл

            mGameField.Init();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            mGameField.Step();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = !timer1.Enabled;
        }

        private void Sen0_Click(object sender, EventArgs e)
        {
            mGameField.CreateZomb(0);
        }

        private void bt_path6_Click(object sender, EventArgs e)
        {
            //Bullet bl = new BulletSimple();
            //bl.Init(mGameField, bt_path6.Location);
            //double angle = Gutils.FindAngleTarget(bl.GetSpeed(), 0, 0, 100, -1, bt_path6.Location);

            //mGameField.CreateBullet(bl, angle);
        }

        private void pn_field_Click(object sender, EventArgs e)
        {
            if (mGameField.CurrentSenPlace != null)
            {
                mGameField.CeaseChoosing();
                return;
            }
        }
    }
}