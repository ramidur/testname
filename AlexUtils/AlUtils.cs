﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlexUtils
{
    public static class AlUtils
    {
        public static List<string> LoadFileToList(string aFileName)
        {
            if (!File.Exists(aFileName))
                return new List<string>();
            var logFile = File.ReadAllLines(aFileName);
            List<string> r = new List<string>(logFile);
            return r;
        }

        public static void SaveListToFile(string aFileName, List<string> aList)
        {
            File.WriteAllLines(aFileName, aList);
        }
    }

    public static class StrTo
    {
        public static int Int(string aS, int aDef=0)
        {
            if (Int32.TryParse(aS, out int x))
                return x;
            return aDef;
        }
    }
}
