﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TowerDef
{
    public class SentryPlace : GameObject
    {
        private Sentry mSn;
        private Sentry mNullSn = new SentryNull();
        public Button mControl;
        public GameField mGameField;

        public override void SelfDelete() { return; }

        public void Init(Button aPlace, GameField aGameField)
        {
            mSn = mNullSn;
            mGameField = aGameField;
            mControl = aPlace;
        }

        public Type GetSentryType()
        {
            return mSn.GetType();
        }

        public void PutSentry(Sentry aSn)
        {
            mSn = aSn;
            mSn.SetLoc(mControl.Location, mGameField);
            mSn.Init();

            ChangeControl();
        }

        private void ChangeControl()
        {
            mControl.BackColor = mSn.mColor;
        }

        public override void Step()
        {
            if (mSn == mNullSn || mGameField.Zombies.Count == 0)
                return;

            mSn.CoolDown();

            FindNearest(mGameField.Zombies);

            if (mSn.GetTarget() == null)
                return;

            if (mSn.SpecialCondition())
                mSn.Act();
        }

        private void FindNearest(List<Zombie> aL)
        {
            int n = 0;

            for (int i = 0; i < aL.Count; i++)
            {
                if (Gutils.FindDist(aL[i].GetLoc(), mControl.Location) < Gutils.FindDist(aL[n].GetLoc(), mControl.Location))
                {
                    n = i;
                }
            }

            if (aL[n].isdeleted)
                return;

            double k = Gutils.FindDist(aL[n].GetLoc(), mControl.Location);
            int a = mSn.GetRange();

            if (k < a)
                mSn.SetTarget(aL[n]);
            else
                mSn.SetTarget(null);
        }
    }

    public abstract class Sentry
    {
        public Color mColor;
        protected int mDamage;
        protected int mCoolDown = 0;
        protected int mCurCoolDown;
        protected Zombie mTarget;
        protected int mRange;
        protected bool mReady = true;
        protected Point mLoc;
        public GameField mGameField;

        public virtual void Shoot(int abulltype)
        {
            Bullet bull = (Bullet)Activator.CreateInstance(mGameField.BulletTypes[abulltype]);
            bull.Init(mGameField, mLoc);

            double TargetAngle = Gutils.FindAngleTarget(bull.GetSpeed(), mTarget.GetSpeedX(), mTarget.GetSpeedY(), mTarget.GetLoc().X + mTarget.GetSize().Width / 2 - mLoc.X, mTarget.GetLoc().Y + mTarget.GetSize().Height / 2 - mLoc.Y);

            bull.SetVector(TargetAngle);
            mGameField.AddGobject(bull);
        }

        public int GetRange()
        {
            return mRange;
        }

        public Zombie GetTarget()
        {
            return mTarget;
        }

        public void SetLoc(Point aLoc, GameField aGameField)
        {
            mLoc = aLoc;
            mGameField = aGameField;
        }

        public void SetTarget(Zombie aTarget)
        {
            mTarget = aTarget;
        }

        public abstract void Init();

        public virtual void Act()
        {
            SetCoolDown();
        }

        public virtual bool SpecialCondition()
        {
            return mReady;
        }

        public virtual void CoolDown()
        {
            if (mCurCoolDown == 0)
                mReady = true;
            else
                mCurCoolDown--;
        }

        protected void SetCoolDown()
        {
            mCurCoolDown = mCoolDown;
            mReady = false;
        }
    }

    public class SentryNull : Sentry
    {
        public override void Init()
        {
            mColor = Color.Silver;
            mDamage = 0;
            mCoolDown = -1;
        }

        public override void Act()
        {
        }

        public override bool SpecialCondition()
        {
            return false;
        }
    }

    public class SentrySimple : Sentry
    {
        public override void Init()
        {
            mColor = Color.MistyRose;
            mRange = 300;
            mCoolDown = 10;
        }

        public override void Act()
        {
            base.Act();

            Shoot(0);
        }
    }

    public class SentrySnipe : Sentry
    {
        public override void Init()
        {
            mColor = Color.PaleGoldenrod;
            mRange = 450;
            mCoolDown = 25;
        }

        public override void Act()
        {
            base.Act();

            Shoot(1);
        }
    }

    public class SentryBoomerAnger : Sentry
    {
        public override void Init()
        {
            mColor = Color.BlueViolet;
            mRange = 300;
            mCoolDown = 35;
        }

        public override void Act()
        {
            base.Act();

            Shoot(2);
        }
    }

    public class SentryFreezing : Sentry
    {
        public override void Init()
        {
            mColor = Color.Turquoise;
            mRange = 300;
            mCoolDown = 12;
        }

        public override void Act()
        {
            base.Act();

            Shoot(3);
        }
    }

    public class SentryCryoGen : Sentry
    {
        public override void Init()
        {
            mColor = Color.Cyan;
            mRange = 300;
            mCoolDown = 15;
        }

        public override void Act()
        {
            base.Act();

            Shoot(4);
        }
    }

    public class SentryCrytalSniper : Sentry
    {
        public override void Init()
        {
            mColor = Color.CornflowerBlue;
            mRange = 450;
            mCoolDown = 25;
        }

        public override void Act()
        {
            base.Act();

            Shoot(5);
        }
    }

    public class SentryFireBall : Sentry
    {
        public override void Init()
        {
            mColor = Color.OrangeRed;
            mRange = 400;
            mCoolDown = 35;
        }

        public override void Act()
        {
            base.Act();

            Bullet bull = (Bullet)Activator.CreateInstance(mGameField.BulletTypes[7]);
            bull.Init(mGameField, mLoc);

            double TargetAngle = Gutils.FindAngleAcceleration((bull.GetSpeed()), (mTarget.GetSpeedX()), (mTarget.GetSpeedY()), mTarget.GetLoc().X + mTarget.GetSize().Width / 2 - mLoc.X, mTarget.GetLoc().Y + mTarget.GetSize().Height / 2 - mLoc.Y, 2);

            bull.SetVector(TargetAngle);
            mGameField.AddGobject(bull);
        }
    }

    public class SentryRocketLauncher : Sentry
    {
        public override void Init()
        {
            mColor = Color.DodgerBlue;
            mRange = 700;
            mCoolDown = 50;
        }

        public override void Act()
        {
            base.Act();

            Bullet bull = (Bullet)Activator.CreateInstance(mGameField.BulletTypes[8]);
            bull.Init(mGameField, mLoc);
            bull.SetVector(0);
            mGameField.AddGobject(bull);
        }
    }
}