﻿namespace TowerDef
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pn_field = new System.Windows.Forms.Panel();
            this.bt_SenType8 = new System.Windows.Forms.Button();
            this.bt_SenType7 = new System.Windows.Forms.Button();
            this.bt_SenType6 = new System.Windows.Forms.Button();
            this.bt_SenType5 = new System.Windows.Forms.Button();
            this.bt_SenType4 = new System.Windows.Forms.Button();
            this.bt_SenType3 = new System.Windows.Forms.Button();
            this.bt_SenType2 = new System.Windows.Forms.Button();
            this.bt_SenType1 = new System.Windows.Forms.Button();
            this.bt_SenType0 = new System.Windows.Forms.Button();
            this.Sen6 = new System.Windows.Forms.Button();
            this.Sen5 = new System.Windows.Forms.Button();
            this.Sen4 = new System.Windows.Forms.Button();
            this.Sen2 = new System.Windows.Forms.Button();
            this.Sen3 = new System.Windows.Forms.Button();
            this.Sen1 = new System.Windows.Forms.Button();
            this.Sen0 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.bt_path7 = new System.Windows.Forms.Button();
            this.bt_path6 = new System.Windows.Forms.Button();
            this.bt_path5 = new System.Windows.Forms.Button();
            this.bt_path4 = new System.Windows.Forms.Button();
            this.bt_path3 = new System.Windows.Forms.Button();
            this.bt_path2 = new System.Windows.Forms.Button();
            this.bt_path1 = new System.Windows.Forms.Button();
            this.bt_path0 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pn_field.SuspendLayout();
            this.SuspendLayout();
            // 
            // pn_field
            // 
            this.pn_field.Controls.Add(this.bt_SenType8);
            this.pn_field.Controls.Add(this.bt_SenType7);
            this.pn_field.Controls.Add(this.bt_SenType6);
            this.pn_field.Controls.Add(this.bt_SenType5);
            this.pn_field.Controls.Add(this.bt_SenType4);
            this.pn_field.Controls.Add(this.bt_SenType3);
            this.pn_field.Controls.Add(this.bt_SenType2);
            this.pn_field.Controls.Add(this.bt_SenType1);
            this.pn_field.Controls.Add(this.bt_SenType0);
            this.pn_field.Controls.Add(this.Sen6);
            this.pn_field.Controls.Add(this.Sen5);
            this.pn_field.Controls.Add(this.Sen4);
            this.pn_field.Controls.Add(this.Sen2);
            this.pn_field.Controls.Add(this.Sen3);
            this.pn_field.Controls.Add(this.Sen1);
            this.pn_field.Controls.Add(this.Sen0);
            this.pn_field.Controls.Add(this.button1);
            this.pn_field.Controls.Add(this.bt_path7);
            this.pn_field.Controls.Add(this.bt_path6);
            this.pn_field.Controls.Add(this.bt_path5);
            this.pn_field.Controls.Add(this.bt_path4);
            this.pn_field.Controls.Add(this.bt_path3);
            this.pn_field.Controls.Add(this.bt_path2);
            this.pn_field.Controls.Add(this.bt_path1);
            this.pn_field.Controls.Add(this.bt_path0);
            this.pn_field.Location = new System.Drawing.Point(24, 23);
            this.pn_field.Margin = new System.Windows.Forms.Padding(6);
            this.pn_field.Name = "pn_field";
            this.pn_field.Size = new System.Drawing.Size(2778, 1269);
            this.pn_field.TabIndex = 0;
            this.pn_field.Click += new System.EventHandler(this.pn_field_Click);
            // 
            // bt_SenType8
            // 
            this.bt_SenType8.BackColor = System.Drawing.SystemColors.Highlight;
            this.bt_SenType8.Enabled = false;
            this.bt_SenType8.Location = new System.Drawing.Point(2092, 29);
            this.bt_SenType8.Margin = new System.Windows.Forms.Padding(6);
            this.bt_SenType8.Name = "bt_SenType8";
            this.bt_SenType8.Size = new System.Drawing.Size(100, 79);
            this.bt_SenType8.TabIndex = 24;
            this.bt_SenType8.Tag = "99";
            this.bt_SenType8.Text = "Null";
            this.bt_SenType8.UseVisualStyleBackColor = false;
            // 
            // bt_SenType7
            // 
            this.bt_SenType7.BackColor = System.Drawing.SystemColors.Highlight;
            this.bt_SenType7.Enabled = false;
            this.bt_SenType7.Location = new System.Drawing.Point(1672, 29);
            this.bt_SenType7.Margin = new System.Windows.Forms.Padding(6);
            this.bt_SenType7.Name = "bt_SenType7";
            this.bt_SenType7.Size = new System.Drawing.Size(100, 79);
            this.bt_SenType7.TabIndex = 23;
            this.bt_SenType7.Tag = "21";
            this.bt_SenType7.Text = "RocketLauncher";
            this.bt_SenType7.UseVisualStyleBackColor = false;
            // 
            // bt_SenType6
            // 
            this.bt_SenType6.BackColor = System.Drawing.SystemColors.Highlight;
            this.bt_SenType6.Enabled = false;
            this.bt_SenType6.Location = new System.Drawing.Point(1430, 29);
            this.bt_SenType6.Margin = new System.Windows.Forms.Padding(6);
            this.bt_SenType6.Name = "bt_SenType6";
            this.bt_SenType6.Size = new System.Drawing.Size(100, 79);
            this.bt_SenType6.TabIndex = 22;
            this.bt_SenType6.Tag = "11";
            this.bt_SenType6.Text = "FireBall";
            this.bt_SenType6.UseVisualStyleBackColor = false;
            // 
            // bt_SenType5
            // 
            this.bt_SenType5.BackColor = System.Drawing.SystemColors.Highlight;
            this.bt_SenType5.Enabled = false;
            this.bt_SenType5.Location = new System.Drawing.Point(1188, 29);
            this.bt_SenType5.Margin = new System.Windows.Forms.Padding(6);
            this.bt_SenType5.Name = "bt_SenType5";
            this.bt_SenType5.Size = new System.Drawing.Size(100, 79);
            this.bt_SenType5.TabIndex = 20;
            this.bt_SenType5.Tag = "11";
            this.bt_SenType5.Text = "CrytalSniper";
            this.bt_SenType5.UseVisualStyleBackColor = false;
            // 
            // bt_SenType4
            // 
            this.bt_SenType4.BackColor = System.Drawing.SystemColors.Highlight;
            this.bt_SenType4.Enabled = false;
            this.bt_SenType4.Location = new System.Drawing.Point(952, 29);
            this.bt_SenType4.Margin = new System.Windows.Forms.Padding(6);
            this.bt_SenType4.Name = "bt_SenType4";
            this.bt_SenType4.Size = new System.Drawing.Size(100, 79);
            this.bt_SenType4.TabIndex = 19;
            this.bt_SenType4.Tag = "31";
            this.bt_SenType4.Text = "CryoGen";
            this.bt_SenType4.UseVisualStyleBackColor = false;
            // 
            // bt_SenType3
            // 
            this.bt_SenType3.BackColor = System.Drawing.SystemColors.Highlight;
            this.bt_SenType3.Enabled = false;
            this.bt_SenType3.Location = new System.Drawing.Point(718, 29);
            this.bt_SenType3.Margin = new System.Windows.Forms.Padding(6);
            this.bt_SenType3.Name = "bt_SenType3";
            this.bt_SenType3.Size = new System.Drawing.Size(100, 79);
            this.bt_SenType3.TabIndex = 18;
            this.bt_SenType3.Tag = "30";
            this.bt_SenType3.Text = "Freezing";
            this.bt_SenType3.UseVisualStyleBackColor = false;
            // 
            // bt_SenType2
            // 
            this.bt_SenType2.BackColor = System.Drawing.SystemColors.Highlight;
            this.bt_SenType2.Enabled = false;
            this.bt_SenType2.Location = new System.Drawing.Point(504, 29);
            this.bt_SenType2.Margin = new System.Windows.Forms.Padding(6);
            this.bt_SenType2.Name = "bt_SenType2";
            this.bt_SenType2.Size = new System.Drawing.Size(100, 79);
            this.bt_SenType2.TabIndex = 17;
            this.bt_SenType2.Tag = "20";
            this.bt_SenType2.Text = "BoomerAnger";
            this.bt_SenType2.UseVisualStyleBackColor = false;
            // 
            // bt_SenType1
            // 
            this.bt_SenType1.BackColor = System.Drawing.SystemColors.Highlight;
            this.bt_SenType1.Enabled = false;
            this.bt_SenType1.Location = new System.Drawing.Point(280, 29);
            this.bt_SenType1.Margin = new System.Windows.Forms.Padding(6);
            this.bt_SenType1.Name = "bt_SenType1";
            this.bt_SenType1.Size = new System.Drawing.Size(100, 79);
            this.bt_SenType1.TabIndex = 16;
            this.bt_SenType1.Tag = "10";
            this.bt_SenType1.Text = "Snipe";
            this.bt_SenType1.UseVisualStyleBackColor = false;
            // 
            // bt_SenType0
            // 
            this.bt_SenType0.BackColor = System.Drawing.SystemColors.Highlight;
            this.bt_SenType0.Enabled = false;
            this.bt_SenType0.Location = new System.Drawing.Point(56, 29);
            this.bt_SenType0.Margin = new System.Windows.Forms.Padding(6);
            this.bt_SenType0.Name = "bt_SenType0";
            this.bt_SenType0.Size = new System.Drawing.Size(100, 79);
            this.bt_SenType0.TabIndex = 15;
            this.bt_SenType0.Tag = "00";
            this.bt_SenType0.Text = "Simple";
            this.bt_SenType0.UseVisualStyleBackColor = false;
            // 
            // Sen6
            // 
            this.Sen6.BackColor = System.Drawing.Color.Silver;
            this.Sen6.Location = new System.Drawing.Point(2250, 248);
            this.Sen6.Margin = new System.Windows.Forms.Padding(6);
            this.Sen6.Name = "Sen6";
            this.Sen6.Size = new System.Drawing.Size(120, 104);
            this.Sen6.TabIndex = 14;
            this.Sen6.Text = "Sentry";
            this.Sen6.UseVisualStyleBackColor = false;
            // 
            // Sen5
            // 
            this.Sen5.BackColor = System.Drawing.Color.Silver;
            this.Sen5.Location = new System.Drawing.Point(1522, 285);
            this.Sen5.Margin = new System.Windows.Forms.Padding(6);
            this.Sen5.Name = "Sen5";
            this.Sen5.Size = new System.Drawing.Size(120, 104);
            this.Sen5.TabIndex = 13;
            this.Sen5.Text = "Sentry";
            this.Sen5.UseVisualStyleBackColor = false;
            // 
            // Sen4
            // 
            this.Sen4.BackColor = System.Drawing.Color.Silver;
            this.Sen4.Location = new System.Drawing.Point(1700, 812);
            this.Sen4.Margin = new System.Windows.Forms.Padding(6);
            this.Sen4.Name = "Sen4";
            this.Sen4.Size = new System.Drawing.Size(120, 104);
            this.Sen4.TabIndex = 12;
            this.Sen4.Text = "Sentry";
            this.Sen4.UseVisualStyleBackColor = false;
            // 
            // Sen2
            // 
            this.Sen2.BackColor = System.Drawing.Color.Silver;
            this.Sen2.Location = new System.Drawing.Point(964, 285);
            this.Sen2.Margin = new System.Windows.Forms.Padding(6);
            this.Sen2.Name = "Sen2";
            this.Sen2.Size = new System.Drawing.Size(120, 104);
            this.Sen2.TabIndex = 11;
            this.Sen2.Text = "Sentry";
            this.Sen2.UseVisualStyleBackColor = false;
            // 
            // Sen3
            // 
            this.Sen3.BackColor = System.Drawing.Color.Silver;
            this.Sen3.Location = new System.Drawing.Point(872, 794);
            this.Sen3.Margin = new System.Windows.Forms.Padding(6);
            this.Sen3.Name = "Sen3";
            this.Sen3.Size = new System.Drawing.Size(120, 104);
            this.Sen3.TabIndex = 10;
            this.Sen3.Text = "Sentry";
            this.Sen3.UseVisualStyleBackColor = false;
            // 
            // Sen1
            // 
            this.Sen1.BackColor = System.Drawing.Color.Silver;
            this.Sen1.Location = new System.Drawing.Point(384, 285);
            this.Sen1.Margin = new System.Windows.Forms.Padding(6);
            this.Sen1.Name = "Sen1";
            this.Sen1.Size = new System.Drawing.Size(120, 104);
            this.Sen1.TabIndex = 9;
            this.Sen1.Text = "Sentry";
            this.Sen1.UseVisualStyleBackColor = false;
            // 
            // Sen0
            // 
            this.Sen0.BackColor = System.Drawing.Color.Silver;
            this.Sen0.Location = new System.Drawing.Point(202, 950);
            this.Sen0.Margin = new System.Windows.Forms.Padding(6);
            this.Sen0.Name = "Sen0";
            this.Sen0.Size = new System.Drawing.Size(120, 104);
            this.Sen0.TabIndex = 1;
            this.Sen0.Text = "Sentry";
            this.Sen0.UseVisualStyleBackColor = false;
            this.Sen0.Click += new System.EventHandler(this.Sen0_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button1.Location = new System.Drawing.Point(28, 1104);
            this.button1.Margin = new System.Windows.Forms.Padding(6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 79);
            this.button1.TabIndex = 8;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bt_path7
            // 
            this.bt_path7.BackColor = System.Drawing.SystemColors.ControlText;
            this.bt_path7.Location = new System.Drawing.Point(2712, 550);
            this.bt_path7.Margin = new System.Windows.Forms.Padding(6);
            this.bt_path7.Name = "bt_path7";
            this.bt_path7.Size = new System.Drawing.Size(40, 231);
            this.bt_path7.TabIndex = 7;
            this.bt_path7.Text = "button8";
            this.bt_path7.UseVisualStyleBackColor = false;
            // 
            // bt_path6
            // 
            this.bt_path6.BackColor = System.Drawing.SystemColors.Desktop;
            this.bt_path6.Location = new System.Drawing.Point(1954, 496);
            this.bt_path6.Margin = new System.Windows.Forms.Padding(6);
            this.bt_path6.Name = "bt_path6";
            this.bt_path6.Size = new System.Drawing.Size(30, 29);
            this.bt_path6.TabIndex = 6;
            this.bt_path6.Text = "button7";
            this.bt_path6.UseVisualStyleBackColor = false;
            this.bt_path6.Click += new System.EventHandler(this.bt_path6_Click);
            // 
            // bt_path5
            // 
            this.bt_path5.BackColor = System.Drawing.SystemColors.ControlText;
            this.bt_path5.Location = new System.Drawing.Point(2372, 1079);
            this.bt_path5.Margin = new System.Windows.Forms.Padding(6);
            this.bt_path5.Name = "bt_path5";
            this.bt_path5.Size = new System.Drawing.Size(30, 29);
            this.bt_path5.TabIndex = 5;
            this.bt_path5.Text = "button6";
            this.bt_path5.UseVisualStyleBackColor = false;
            // 
            // bt_path4
            // 
            this.bt_path4.BackColor = System.Drawing.SystemColors.ControlText;
            this.bt_path4.Location = new System.Drawing.Point(1370, 1079);
            this.bt_path4.Margin = new System.Windows.Forms.Padding(6);
            this.bt_path4.Name = "bt_path4";
            this.bt_path4.Size = new System.Drawing.Size(30, 29);
            this.bt_path4.TabIndex = 4;
            this.bt_path4.Text = "button5";
            this.bt_path4.UseVisualStyleBackColor = false;
            // 
            // bt_path3
            // 
            this.bt_path3.BackColor = System.Drawing.SystemColors.Desktop;
            this.bt_path3.Location = new System.Drawing.Point(1252, 583);
            this.bt_path3.Margin = new System.Windows.Forms.Padding(6);
            this.bt_path3.Name = "bt_path3";
            this.bt_path3.Size = new System.Drawing.Size(30, 29);
            this.bt_path3.TabIndex = 3;
            this.bt_path3.Text = "button4";
            this.bt_path3.UseVisualStyleBackColor = false;
            // 
            // bt_path2
            // 
            this.bt_path2.BackColor = System.Drawing.SystemColors.ControlText;
            this.bt_path2.Location = new System.Drawing.Point(592, 617);
            this.bt_path2.Margin = new System.Windows.Forms.Padding(6);
            this.bt_path2.Name = "bt_path2";
            this.bt_path2.Size = new System.Drawing.Size(30, 29);
            this.bt_path2.TabIndex = 2;
            this.bt_path2.Text = "button3";
            this.bt_path2.UseVisualStyleBackColor = false;
            // 
            // bt_path1
            // 
            this.bt_path1.BackColor = System.Drawing.SystemColors.ControlText;
            this.bt_path1.Location = new System.Drawing.Point(728, 1079);
            this.bt_path1.Margin = new System.Windows.Forms.Padding(6);
            this.bt_path1.Name = "bt_path1";
            this.bt_path1.Size = new System.Drawing.Size(30, 29);
            this.bt_path1.TabIndex = 1;
            this.bt_path1.Text = "button2";
            this.bt_path1.UseVisualStyleBackColor = false;
            // 
            // bt_path0
            // 
            this.bt_path0.BackColor = System.Drawing.SystemColors.ControlText;
            this.bt_path0.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bt_path0.Location = new System.Drawing.Point(56, 515);
            this.bt_path0.Margin = new System.Windows.Forms.Padding(6);
            this.bt_path0.Name = "bt_path0";
            this.bt_path0.Size = new System.Drawing.Size(80, 77);
            this.bt_path0.TabIndex = 0;
            this.bt_path0.Text = "button1";
            this.bt_path0.UseVisualStyleBackColor = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2826, 1315);
            this.Controls.Add(this.pn_field);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.pn_field.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pn_field;
        private System.Windows.Forms.Button bt_path7;
        private System.Windows.Forms.Button bt_path6;
        private System.Windows.Forms.Button bt_path5;
        private System.Windows.Forms.Button bt_path4;
        private System.Windows.Forms.Button bt_path3;
        private System.Windows.Forms.Button bt_path2;
        private System.Windows.Forms.Button bt_path1;
        private System.Windows.Forms.Button bt_path0;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Sen6;
        private System.Windows.Forms.Button Sen5;
        private System.Windows.Forms.Button Sen4;
        private System.Windows.Forms.Button Sen2;
        private System.Windows.Forms.Button Sen3;
        private System.Windows.Forms.Button Sen1;
        private System.Windows.Forms.Button Sen0;
        private System.Windows.Forms.Button bt_SenType1;
        private System.Windows.Forms.Button bt_SenType0;
        private System.Windows.Forms.Button bt_SenType2;
        private System.Windows.Forms.Button bt_SenType3;
        private System.Windows.Forms.Button bt_SenType4;
        private System.Windows.Forms.Button bt_SenType5;
        private System.Windows.Forms.Button bt_SenType6;
        private System.Windows.Forms.Button bt_SenType7;
        private System.Windows.Forms.Button bt_SenType8;
    }
}

