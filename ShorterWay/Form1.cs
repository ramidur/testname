﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ShorterWay
{
    public partial class Form1 : Form
    {
        Graphics g;
        string s;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<int> parametres = new List<int>();
            // 0, 1 - width and height
            // 2, 3 - point 1
            // 4, 5 - point 2

            using (StreamReader sr = new StreamReader(@"C:\\a\\info.txt"))
                s = sr.ReadToEnd();

            for (int i = 0; i < 3; i++)
            {
                int d = s.IndexOf(")") - s.IndexOf("(");

                string ss = s.Substring(s.IndexOf("(") + 1, d - 1);
                s = s.Remove(0, d + s.IndexOf("(") + 2);

                parametres.Add(Convert.ToInt32(ss.Substring(0, ss.IndexOf(" "))));
                parametres.Add(Convert.ToInt32(ss.Substring(ss.IndexOf(" "))));
            }

            panel1.Size = new Size(parametres[0], parametres[1]);

            Bitmap b = new Bitmap(@"C:\\a\\map.bmp");

            g = this.panel1.CreateGraphics();
            g.DrawImage(b, 0, 0);
        }

        private void bt_edit_Click(object sender, EventArgs e)
        {
            String[] param = new string[6];

            param[0] = (tb_panel.Text.Substring(0, tb_panel.Text.IndexOf(" ")));
            param[1] = (tb_panel.Text.Substring(tb_panel.Text.IndexOf(" ")));
            param[2] = (tb_point1.Text.Substring(0 ,tb_point1.Text.IndexOf(" ")));
            param[3] = (tb_point1.Text.Substring(tb_point1.Text.IndexOf(" ")));
            param[4] = (tb_point2.Text.Substring(0 ,tb_point2.Text.IndexOf(" ")));
            param[5] = (tb_point2.Text.Substring(tb_point2.Text.IndexOf(" ")));

            using (StreamWriter sw = new StreamWriter((@"C:\\a\\info.txt"), false))
            {
                sw.WriteLine("panel(" + param[0] + " " + param[1] + ")" + " point1(" + param[2] + " " + param[3] + ")" + " point2(" + param[4] + " " + param[5] + ")");
            }
        }
    }
}
