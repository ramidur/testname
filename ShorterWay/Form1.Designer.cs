﻿namespace ShorterWay
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.bt_set = new System.Windows.Forms.Button();
            this.tb_panel = new System.Windows.Forms.TextBox();
            this.tb_point1 = new System.Windows.Forms.TextBox();
            this.tb_point2 = new System.Windows.Forms.TextBox();
            this.width_height = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bt_edit = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(888, 535);
            this.panel1.TabIndex = 0;
            // 
            // bt_set
            // 
            this.bt_set.Location = new System.Drawing.Point(3, 191);
            this.bt_set.Name = "bt_set";
            this.bt_set.Size = new System.Drawing.Size(92, 23);
            this.bt_set.TabIndex = 0;
            this.bt_set.Text = "set";
            this.bt_set.UseVisualStyleBackColor = true;
            this.bt_set.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_panel
            // 
            this.tb_panel.Location = new System.Drawing.Point(14, 12);
            this.tb_panel.Name = "tb_panel";
            this.tb_panel.Size = new System.Drawing.Size(183, 20);
            this.tb_panel.TabIndex = 1;
            this.tb_panel.Text = "685 500";
            // 
            // tb_point1
            // 
            this.tb_point1.Location = new System.Drawing.Point(14, 38);
            this.tb_point1.Name = "tb_point1";
            this.tb_point1.Size = new System.Drawing.Size(183, 20);
            this.tb_point1.TabIndex = 3;
            this.tb_point1.Text = "20 470";
            // 
            // tb_point2
            // 
            this.tb_point2.Location = new System.Drawing.Point(14, 64);
            this.tb_point2.Name = "tb_point2";
            this.tb_point2.Size = new System.Drawing.Size(183, 20);
            this.tb_point2.TabIndex = 4;
            this.tb_point2.Text = "600 20";
            // 
            // width_height
            // 
            this.width_height.AutoSize = true;
            this.width_height.Location = new System.Drawing.Point(203, 15);
            this.width_height.Name = "width_height";
            this.width_height.Size = new System.Drawing.Size(64, 13);
            this.width_height.TabIndex = 5;
            this.width_height.Text = "width height";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bt_edit);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.tb_point2);
            this.panel2.Controls.Add(this.width_height);
            this.panel2.Controls.Add(this.bt_set);
            this.panel2.Controls.Add(this.tb_panel);
            this.panel2.Controls.Add(this.tb_point1);
            this.panel2.Location = new System.Drawing.Point(1026, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(347, 227);
            this.panel2.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(203, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "x2 y2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(203, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "x1 y1";
            // 
            // bt_edit
            // 
            this.bt_edit.Location = new System.Drawing.Point(101, 191);
            this.bt_edit.Name = "bt_edit";
            this.bt_edit.Size = new System.Drawing.Size(114, 23);
            this.bt_edit.TabIndex = 9;
            this.bt_edit.Text = "edit";
            this.bt_edit.UseVisualStyleBackColor = true;
            this.bt_edit.Click += new System.EventHandler(this.bt_edit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1451, 702);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bt_set;
        private System.Windows.Forms.TextBox tb_panel;
        private System.Windows.Forms.TextBox tb_point1;
        private System.Windows.Forms.TextBox tb_point2;
        private System.Windows.Forms.Label width_height;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bt_edit;
    }
}

