﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FillHdc
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            bitmap = new Bitmap(panel1.Width, panel1.Height);
        }

        Bitmap bitmap;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Graphics g = Graphics.FromImage(bitmap);
                Brush brush = new SolidBrush(Color.DarkGreen);

                Point[] points = new Point[4];

                points[0] = new Point(e.X - 10, e.Y - 10);
                points[1] = new Point(e.X + 10, e.Y - 10);
                points[2] = new Point(e.X + 10, e.Y + 10);
                points[3] = new Point(e.X - 10, e.Y + 10);

                g.FillPolygon(brush, points);

                panel1.CreateGraphics().DrawImage(bitmap, 0, 0);
            }
        }

        Color mFillColor;

        private void Fill(int x, int y)
        {
            mFillColor = bitmap.GetPixel(x, y);
            mCountCalls = 0;

            DateTime time = DateTime.Now;
            CheckPaintAndSearch3(x, y);
            double ms = (DateTime.Now - time).TotalMilliseconds;
            Text = "" + ms;

            panel1.CreateGraphics().DrawImage(bitmap, 0, 0);
        }

        private void CheckPaintAndSearch3(int x, int y)
        {
            mPoints.Clear();
            CheckAndAddPoint(x, y);

            while (mPoints.Count > 0)
            {
                vizualize();

                int idx = 0;// mPoints.Count - 1;
                int xx = mPoints[idx].x;
                int yy = mPoints[idx].y;
                mPoints.RemoveAt(idx);

                bitmap.SetPixel(xx, yy, Color.Red);

                CheckAndAddPoint(xx + 1, yy);
                CheckAndAddPoint(xx - 1, yy);
                CheckAndAddPoint(xx, yy + 1);
                CheckAndAddPoint(xx, yy - 1);
            }
        }

        private void CheckPaintAndSearch2(int x, int y)
        {
            mPoints.Add((x, y));

            while (mPoints.Count > 0)
            {
                int idx = mPoints.Count - 1;
                int xx = mPoints[idx].x;
                int yy = mPoints[idx].y;
                mPoints.RemoveAt(idx);

                bitmap.SetPixel(xx, yy, Color.Red);

                int step = 1;
                int cur = 1;
                bool bup = true;
                bool bdown = true;

                for (int i = 0; i < bitmap.Width; i++)
                {
                    int nx = xx + cur;
                    if (nx < 0 || nx >= bitmap.Width || bitmap.GetPixel(xx + cur, yy) != mFillColor)
                    {
                        if (cur < 0)
                            break;

                        step = -1;
                        cur = -1;
                        continue;
                    }

                    vizualize();
                    bitmap.SetPixel(xx + cur, yy, Color.Red);

                    if (yy + 1 < bitmap.Height && bitmap.GetPixel(xx + cur, yy + 1) == mFillColor)
                    {
                        if (bdown)
                        {
                            CheckAndAddPoint(xx + cur, yy + 1);
                            bdown = false;
                        }
                    }
                    else
                    {
                        bdown = true;
                    }

                    if (yy - 1 >= 0 && bitmap.GetPixel(xx + cur, yy - 1) == mFillColor)
                    {
                        if (bup)
                        {
                            CheckAndAddPoint(xx + cur, yy - 1);
                            bup = false;
                        }
                    }
                    else
                    {
                        bup = true;
                    }

                    cur += step;
                }
            }
        }

        int mCountCalls = 0;

        List<(int x, int y)> mPoints = new List<(int, int)>();

        public void vizualize()
        {
            mCountCalls++;

            if (mCountCalls != 400)
                return;

            mCountCalls = 0;
            panel1.CreateGraphics().DrawImage(bitmap, 0, 0);
            Application.DoEvents();
        }

        void CheckAndAddPoint(int x, int y)
        {
            if (x < 0 || y < 0 || x >= bitmap.Width || y >= bitmap.Height)
                return;
            var c = bitmap.GetPixel(x, y);
            if (c != mFillColor)
                return;
            if (mPoints.Contains((x, y)))
                return;
            mPoints.Add((x, y));
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Fill(e.X, e.Y);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}