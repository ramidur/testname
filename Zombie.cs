﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TowerDef
{
    public abstract class Zombie : GameObject
    {
        protected Control mPath;
        protected Control mControl;
        GameField mGameField;
        protected int mWidth, mHeight;
        protected int pathNum;
        protected double mspeedX = 1;
        protected double mspeedY = 0;
        public double mspeed;
        protected Color mColor;
        public List<DurationEffect> mEffects;
        protected double angl;
        public bool CantBeHit = false;
        protected double mDamageMultiplier = 1;

        public override void SelfDelete()
        {
            mControl.Dispose();
        }

        public virtual void Init(GameField aGameField)
        {
            pathNum = 0;
            mEffects = new List<DurationEffect>();
            mGameField = aGameField;
            mPath = mGameField.GetPath(pathNum);
            mGameField.AddGobject(this);
            CreateControl();
        }

        public virtual void CreateControl()
        {
            Button bt = new Button();
            mControl = bt;
            mGameField.mField.Controls.Add(mControl);
            mWidth = 50;
            mHeight = 50;
            mControl.Size = new Size(mWidth, mHeight);
            mControl.Location = new Point(mGameField.GetPath(0).Location.X, mGameField.GetPath(0).Location.Y);
            mControl.BackColor = mColor;
            mControl.Text = "z";
            mControl.BringToFront();
        }

        public override void Step()
        {
            if (CheckPosition())
                FindPath();

            EffectsAct();

            Move();
        }

        protected void EffectsAct()
        {
            for (int i = mEffects.Count - 1; i >= 0; i--)
            {
                mEffects[i].DurAffect();
            }
        }

        public void SetDamageMultiplier(double aDmgMplt)
        {
            mDamageMultiplier = aDmgMplt;
        }

        public void ChangeColor(Color aColor)
        {
            mControl.BackColor = aColor;
        }

        public void ChangeColor()
        {
            mControl.BackColor = mColor;
        }

        public void TakeEffect(DurationEffect aEffect)
        {
            // TODO передать тип как переменную и проверить нормально

            if (!aEffect.StackAbility())
            {
                foreach (var elem in mEffects)
                {
                    if (aEffect.GetType() == elem.GetType())
                    {
                        elem.TimeReset();
                        return;
                    }
                }
            }

            mEffects.Add(aEffect);
            aEffect.Init(mGameField, this);
        }

        public bool CheckPosition()
        {
            if ((GetLoc().X + mWidth > mPath.Location.X && GetLoc().X < mPath.Location.X + mPath.Size.Width) && (GetLoc().Y + mHeight > mPath.Location.Y && GetLoc().Y < mPath.Location.Y + mPath.Size.Height))
            {
                if (pathNum >= mGameField.pathbts.Count - 1)
                {
                    mGameField.GameStop();
                    return false;
                }

                pathNum += 1;
                mPath = mGameField.GetPath(Convert.ToInt32(pathNum));
                return true;
            }
            else { return false; }
        }

        public void FindPath()
        {
            angl = Gutils.PointsToAngle(GetLoc().X + GetSize().Width, GetLoc().Y + GetSize().Height, mPath.Location.X + mPath.Width, mPath.Location.Y + mPath.Height);

            SpeedChange();
        }

        public void SpeedChange()
        {
            mspeedX = mspeed * Math.Cos(angl);
            mspeedY = mspeed * Math.Sin(angl);
        }

        public virtual void Move()
        {
            mControl.Left += Convert.ToInt32(mspeedX);
            mControl.Top += Convert.ToInt32(mspeedY);
        }

        public void Damaged(int aDamage)
        {
            hp -= Convert.ToInt32(aDamage * mDamageMultiplier);

            CheckHp();
        }

        public virtual void CheckHp()
        {
            if (hp <= 0)
            {
                isdeleted = true;
                return;
            }
        }

        public double GetSpeedX()
        {
            return mspeedX;
        }

        public double GetSpeedY()
        {
            return mspeedY;
        }

        public Point GetLoc() { return mControl.Location; }

        public Size GetSize() { return mControl.Size; }

        public void SetHp(int ahp)
        {
            hp = ahp;
        }
    }

    public class ZombieNorm : Zombie
    {
        public override void Init(GameField aGameField)
        {
            hp = 100;
            mspeed = 6;
            mColor = Color.Green;

            base.Init(aGameField);
        }
    }

    public class ZombieArmored : Zombie
    {
        bool armored = true;

        public override void Init(GameField aGameField)
        {
            hp = 200;
            mspeed = 6;

            base.Init(aGameField);
        }

        public override void CheckHp()
        {
            if (armored && hp < 6)
            {
                armored = false;
                ChangeColor(Color.Green);
            }

            base.CheckHp();
        }
    }

    public class ZombieBig : Zombie
    {
        public override void Init(GameField aGameField)
        {
            hp = 350;
            mspeed = 3;
            mColor = Color.Green;

            base.Init(aGameField);

            mControl.Size = new Size(GetSize().Width + 10, GetSize().Height + 10);
            mControl.Left += 5;
            mControl.Top += 5;
        }
    }


    public abstract class ZombieBoss : Zombie
    {
        GameField mGameField;
        protected int mSizeX, mSizeY;

        public override void Init(GameField aGameField)
        {
            mGameField = aGameField;
            mGameField.AddGobject(this);
            mPath = mGameField.pathbts[7];
            CreateControl();
        }

        public override void CreateControl()
        {
            Button bt = new Button();
            mControl = bt;
            mGameField.mField.Controls.Add(mControl);
            mWidth = mSizeX;
            mHeight = mSizeY;
            mControl.Size = new Size(mWidth, mHeight);
            mControl.Location = new Point(mGameField.GetPath(0).Location.X, mGameField.GetPath(0).Location.Y);
            mControl.BackColor = mColor;
            mControl.BringToFront();
            mControl.Text = "Z";

            double angl = Gutils.PointsToAngle(mControl.Location.X, mControl.Location.Y + GetSize().Height, mPath.Location.X, mPath.Location.Y);

            mspeedX = mspeed * Math.Cos(angl);
            mspeedY = mspeed * Math.Sin(angl);
        }

        public override void Step()
        {
            Move();

            if ((GetLoc().X + mWidth > mPath.Location.X && GetLoc().X < mPath.Location.X + mPath.Size.Width) && (GetLoc().Y + mHeight > mPath.Location.Y && GetLoc().Y < mPath.Location.Y + mPath.Size.Height))
            {
                mGameField.GameStop();
            }
        }
    }

    public class ZombieBossFat : ZombieBoss
    {
        public override void Init(GameField aGameField)
        {
            hp = 700;
            mspeed = 3;
            mColor = Color.DarkSeaGreen;
            mSizeX = 80;
            mSizeY = 80;

            base.Init(aGameField);
        }
    }
}