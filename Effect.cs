﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerDef
{
    public abstract class Effect
    {
        public int mDuration;
        protected int mTime = 0;
        protected GameField mGameField;
        protected Zombie mVictim;

        public virtual void Init(GameField aGameField, Zombie aVictim)
        {
            mGameField = aGameField;
            mVictim = aVictim;
        }

        public abstract bool StackAbility();
    }

    public abstract class DurationEffect : Effect
    {
        public bool CantStack;

        public override void Init(GameField aGameField, Zombie aVictim)
        {
            base.Init(aGameField, aVictim);
        }
        public virtual void DurAffect()
        {
            mTime++;

            CheckTime();
        }

        public virtual void CheckTime()
        {
            if (mTime >= mDuration)
            {
                SelfDelete();
            }
        }

        protected abstract void ReturnAffects();

        public virtual void SelfDelete()
        {
            ReturnAffects();

            mVictim.mEffects.Remove(this);
        }

        public void TimeReset()
        {
            mTime = 0;
        }
    }

    public class Frozen : DurationEffect
    {
        public override void Init(GameField aGameField, Zombie aVictim)
        {
            base.Init(aGameField, aVictim);

            mDuration = 45;
            mVictim.mspeed /= 2;
            mVictim.SpeedChange();
            mVictim.ChangeColor(Color.DarkCyan);
        }

        public override bool StackAbility()
        {
            return false;
        }

        protected override void ReturnAffects()
        {
            mVictim.mspeed *= 2;
            mVictim.SpeedChange();
            mVictim.ChangeColor();
        }
    }

    public class Burning : DurationEffect
    {
        public override void Init(GameField aGameField, Zombie aVictim)
        {
            base.Init(aGameField, aVictim);

            mDuration = 35;
            mVictim.ChangeColor(Color.Red);
        }

        public override bool StackAbility()
        {
            return false;
        }

        public override void DurAffect()
        {
            base.DurAffect();

            mVictim.Damaged(1);
        }

        protected override void ReturnAffects()
        {
            mVictim.ChangeColor();
        }
    }

    public class Glacial : DurationEffect
    {
        bool Warm = true;

        public override void Init(GameField aGameField, Zombie aVictim)
        {
            base.Init(aGameField, aVictim);

            if (aVictim.GetType() == typeof(ZombieBoss))
            {
                aVictim.Damaged(1);
                aVictim.mEffects.Remove(this);
                return;
            }

            aVictim.SetDamageMultiplier(0.2);
            mVictim.ChangeColor(Color.MidnightBlue);
            mVictim.mspeed /= 1.5;
            mVictim.SpeedChange();

            if (mVictim.mspeed <= 0.5)
            {
                Warm = false;
                aVictim.mspeed = 0;
            }
        }

        public override void DurAffect()
        {
            if (Warm)
                return;

            mVictim.Damaged(1);
        }

        protected override void ReturnAffects()
        {
            mVictim.SetHp(0);
        }

        public override bool StackAbility()
        {
            return true;
        }
    }

    public class Crystalized : DurationEffect
    {
        public override void Init(GameField aGameField, Zombie aVictim)
        {
            base.Init(aGameField, aVictim);

            mDuration = 15;
            mVictim.mspeed /= 2;
            mVictim.SpeedChange();
            mVictim.ChangeColor(Color.BlueViolet);
            mVictim.SetDamageMultiplier(1.5);
        }

        public override bool StackAbility()
        {
            return false;
        }

        protected override void ReturnAffects()
        {
            mVictim.mspeed *= 2;
            mVictim.SpeedChange();
            mVictim.ChangeColor();
            mVictim.SetDamageMultiplier(1);
        }
    }
}