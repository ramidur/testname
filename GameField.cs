﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TowerDef
{
    public class GameField
    {
        public Panel mField;
        private Timer timer1;
        public SentryPlace CurrentSenPlace;

        public List<Control> pathbts = new List<Control>();
        private List<GameObject> GObjects = new List<GameObject>();
        private List<Type> ZombTypes = new List<Type>();
        private List<Type> SenTypes = new List<Type>();
        public List<Button> SenPlaces = new List<Button>();
        public List<Button> ChoosBttns = new List<Button>();
        public List<SentryPlace> SentriesPl = new List<SentryPlace>();
        public List<Zombie> Zombies = new List<Zombie>();
        public List<Type> BulletTypes = new List<Type>();

        public GameField(Panel afield, Timer atime)
        {
            mField = afield;
            timer1 = atime;
        }

        public void Init()
        {
            SetZombTypes();
            SetSenTypes();
            SetChooseSenButtons();
            GetSentryTypesButtons();
            CreateSentriesPl();
            SetBulletTypes();
        }

        private void SetBulletTypes()
        {
            BulletTypes.Add(typeof(BulletSimple));
            BulletTypes.Add(typeof(BulletHeavy));
            BulletTypes.Add(typeof(BulletBoomerang));
            BulletTypes.Add(typeof(BulletFreeze));
            BulletTypes.Add(typeof(BulletCryo));
            BulletTypes.Add(typeof(BulletCrystal));
            BulletTypes.Add(typeof(ShardCrystal));
            BulletTypes.Add(typeof(BulletFireBall));
            BulletTypes.Add(typeof(BulletRocket));
            BulletTypes.Add(typeof(ExplosionParticles));
        }

        private void CreateSentriesPl()
        {
            SentryPlace sp;

            for (int i = 0; i < SenPlaces.Count; i++)
            {
                sp = new SentryPlace();
                SentriesPl.Add(sp);
                GObjects.Add(sp);
                sp.Init(SenPlaces[i], this);
            }
        }

        public void GetSentryTypesButtons()
        {
            foreach (var elem in ChoosBttns)
            {
                elem.MouseClick += SetSenType;
            }
        }

        private void SetSenType(object sender, MouseEventArgs e)
        {
            Button bt = sender as Button;
            int chosen = Convert.ToInt32(bt.Name.Substring(bt.Name.Length - 1));

            Sentry currSen = (Sentry)Activator.CreateInstance(SenTypes[chosen]);
            CurrentSenPlace.PutSentry(currSen);

            CeaseChoosing();
        }

        public void SetChooseSenButtons()
        {
            foreach (var elem in SenPlaces)
            {
                elem.MouseClick += GetCurrentSenPlace;
            }
        }

        private void GetCurrentSenPlace(object sender, MouseEventArgs e)
        {
            Button bt = sender as Button;
            int number = SenPlaces.IndexOf(bt);

            if (CurrentSenPlace == SentriesPl[number])
            {
                CeaseChoosing();
                return;
            }

            CurrentSenPlace = SentriesPl[number];

            ShowPossibleSenTypes();
        }

        private void ShowPossibleSenTypes()
        {
            List<Button> possible = new List<Button>();

            int i = SenTypes.IndexOf((CurrentSenPlace.GetSentryType()));

            string s = ChoosBttns[i].Tag.ToString();

            foreach (var elem in ChoosBttns)
            {
                if (elem == ChoosBttns[i])
                    continue;

                //if (elem.Tag.ToString().Substring(0,1) == s.ToString().Substring(0, 1))
                //{
                //    if (Convert.ToInt32(elem.Tag.ToString().Substring(1, 1)) == (Convert.ToInt32(s.Substring(1, 1)) + 1))
                //    {
                //        elem.Enabled = true;
                //    }

                //    possible.Add(elem);
                //}

                elem.Enabled = true;
                possible.Add(elem);
            }

            possible.Add(ChoosBttns[8]);

            int step = ChoosBttns[1].Width + 10;
            int start = CurrentSenPlace.mControl.Location.X - (((step) * possible.Count - 10) / 2);

            if (start < 20)
                start = 20;// (CurrentSenPlace.mControl.Location.Y - 10) / 2;

            for (int x = 0; x < possible.Count; x++)
            {
                possible[x].Location = new Point(start + step * x, CurrentSenPlace.mControl.Location.Y - 45);
            }
        }

        public void CeaseChoosing()
        {
            foreach (var elem in ChoosBttns)
            {
                elem.Location = new Point(0, 1300);
                elem.Enabled = false;
            }

            CurrentSenPlace = null;
        }

        public void GameStop()
        {
            timer1.Enabled = false;
        }

        public Control GetPath(int anumber)
        {
            return pathbts[anumber];
        }

        private void SetZombTypes()
        {
            ZombTypes.Add(typeof(ZombieNorm));
            ZombTypes.Add(typeof(ZombieArmored));
            ZombTypes.Add(typeof(ZombieBig));
            ZombTypes.Add(typeof(ZombieBossFat));
        }

        private void SetSenTypes()
        {
            SenTypes.Add(typeof(SentrySimple));
            SenTypes.Add(typeof(SentrySnipe));
            SenTypes.Add(typeof(SentryBoomerAnger));
            SenTypes.Add(typeof(SentryFreezing));
            SenTypes.Add(typeof(SentryCryoGen));
            SenTypes.Add(typeof(SentryCrytalSniper));
            SenTypes.Add(typeof(SentryFireBall));
            SenTypes.Add(typeof(SentryRocketLauncher));
            SenTypes.Add(typeof(SentryNull));
        }

        public void AddGobject(GameObject gb)
        {
            GObjects.Add(gb);
        }

        public void CreateZomb(int anum)
        {
            Zombie zb = (Zombie)Activator.CreateInstance(ZombTypes[anum]);
            zb.Init(this);
        }

        public void Step()
        {
            Zombies.Clear();
            for (int i = GObjects.Count - 1; i >= 0; --i)
            {
                if (GObjects[i].isdeleted)
                {
                    GObjects[i].SelfDelete();
                    GObjects.RemoveAt(i);
                }
                else
                if (GObjects[i] is Zombie)
                    Zombies.Add(GObjects[i] as Zombie);
            }

            for (int i = 0; i < GObjects.Count; i++)
            {
                GObjects[i].Step();
            }
        }
    }

    public abstract class GameObject
    {
        public bool isdeleted = false;
        protected int hp;
        public abstract void SelfDelete();

        public abstract void Step();
    }
}